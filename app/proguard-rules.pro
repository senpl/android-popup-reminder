# About Libraries
-keep class .R
-keep class **.R$* {
    <fields>;
}

# Crashlytics
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception