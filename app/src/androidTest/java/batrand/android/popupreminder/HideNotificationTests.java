package batrand.android.popupreminder;

import android.content.Context;
import androidx.test.InstrumentationRegistry;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import batrand.android.popupreminder.models.Notification;
import batrand.android.popupreminder.models.reminder.Reminder;
import batrand.android.popupreminder.services.alarm.AlarmService;
import batrand.android.popupreminder.services.alarm.IAlarmService;
import batrand.android.popupreminder.services.formatter.FormatService;
import batrand.android.popupreminder.services.log.Log;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.popup.PopupService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.services.reminder.ReminderService;
import batrand.android.popupreminder.views.ReminderDetailActivity;

//import android.util.Log;

/**
 * Created by senpl on 2017-05-09.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class HideNotificationTests {

    private int mWait = 3; // Seconds to wait

    private long secondsFromNow(int seconds) {
        return System.currentTimeMillis() + (seconds * 1000);
    }

    private String mId;
    private Reminder mReminder;
    ReminderDetailActivity rda;
    private IReminderService mReminderService;
    private IAlarmService mAlarmService;
    private IPopupService mPopupService;
    private Context mContext;

    @Before
    public void setup() {
        mAlarmService = new AlarmService();
        mPopupService = new PopupService();

        mReminderService = new ReminderService(mAlarmService, mPopupService, new Log(), new FormatService());

        mContext = InstrumentationRegistry.getTargetContext();

        mReminder = new Reminder();
        mId = mReminder.id();
        mReminder.setAsTrigger(secondsFromNow(mWait));
    }

    @After
    public void cleanup() {
        mReminderService.unregisterReminder(mContext, mId);
        mAlarmService.cancelAlarm(mContext, mId);
    }


    @Test
    public void showNotificationsList() throws InterruptedException {
        Log log1 = new Log();
        log1.log("test");
        Context contextMy = InstrumentationRegistry.getTargetContext();

//        mPopupService.popup(contextMy, "Test");
//        mPopupService.
        List<Notification> listaNotifikacjiWyczyszczonaPoUkryciu = mReminderService.getAllNotifications();
        assert (listaNotifikacjiWyczyszczonaPoUkryciu.size() > 0);
        mReminderService.hideNotificationsWithHideDate();
        assert (mReminderService.getAllNotifications().size() == 0);

        log1.log("test");
    }

}
