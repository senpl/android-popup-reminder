package batrand.android.popupreminder;

import android.content.Context;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import batrand.android.popupreminder.services.alarm.AlarmService;
import batrand.android.popupreminder.services.alarm.IAlarmService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by batra on 2017-05-03.
 */
@RunWith(AndroidJUnit4.class)
public class AlarmTests {

    private IAlarmService mAlarmService;
    private Context mContext;
    private String mId = UUID.randomUUID().toString();
    private long tenSecondsFromNow() { return System.currentTimeMillis() + mTenSeconds; }
    private long mTenSeconds = 10000;

    @Before
    public void setup() {
        mContext = InstrumentationRegistry.getTargetContext();
        mAlarmService = new AlarmService();
    }

    @After
    public void cancelAlarm() {
        mAlarmService.cancelAlarm(mContext, mId);
    }

    @Test
    public void canSetAlarm() {
        assertFalse(mAlarmService.isAlarmSet(mContext, mId));
        mAlarmService.setAlarm(mContext, mId, tenSecondsFromNow(), null);
        assertTrue(mAlarmService.isAlarmSet(mContext, mId));
    }

    @Test
    public void canCancelAlarm() {
        mAlarmService.setAlarm(mContext, mId, tenSecondsFromNow(), null);
        assertTrue(mAlarmService.isAlarmSet(mContext, mId));

        mAlarmService.cancelAlarm(mContext, mId);
        assertFalse(mAlarmService.isAlarmSet(mContext, mId));
    }
}
