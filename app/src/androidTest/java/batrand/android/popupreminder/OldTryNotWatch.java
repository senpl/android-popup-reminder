package batrand.android.popupreminder;

import android.content.Context;
import androidx.test.InstrumentationRegistry;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import batrand.android.popupreminder.models.reminder.Reminder;
import batrand.android.popupreminder.services.alarm.AlarmService;
import batrand.android.popupreminder.services.alarm.IAlarmService;
import batrand.android.popupreminder.services.formatter.FormatService;
//import android.util.Log;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.services.reminder.ReminderService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import batrand.android.popupreminder.services.log.Log;
import batrand.android.popupreminder.views.ReminderDetailActivity;

/**
 * Created by batra on 2017-05-09.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class OldTryNotWatch {

    private int mWait = 3; // Seconds to wait
    private long secondsFromNow(int seconds) { return System.currentTimeMillis() + (seconds*1000); }
    private String mId;
    private Reminder mReminder;
    ReminderDetailActivity rda;
    private IReminderService mReminderService;
    private IAlarmService mAlarmService;
    private IPopupService mPopupService;
    private Context mContext;

    @Before
    public void setup() {
        mAlarmService = new AlarmService();
        mPopupService = mock(IPopupService.class);

        mReminderService = new ReminderService(mAlarmService, mPopupService, new Log(), new FormatService());

        mContext = InstrumentationRegistry.getTargetContext();

        mReminder = new Reminder();
        mId = mReminder.id();
        mReminder.setAsTrigger(secondsFromNow(mWait));
    }

    @After
    public void cleanup() {
        mReminderService.unregisterReminder(mContext, mId);
        mAlarmService.cancelAlarm(mContext, mId);
    }

//    @Test
    public void reminderTriggersOnTime() throws InterruptedException {
//        rda.onView
        mReminderService.registerReminder(mContext, mReminder);

        Reminder reminder = mReminderService.getReminder(mId);
        assertFalse(reminder.isTriggered());

        Thread.sleep(mWait+7000); // Allow Realm to sync.

        Reminder reminderRefetched = mReminderService.getReminder(mId);
        assertTrue(reminderRefetched.isTriggered());
    }

    @Test
    public void showNotificationsList() throws InterruptedException {
        //open app Notifications


        Log log1=new Log();
//        log.
        log1.log("test");
        mReminderService.registerReminder(mContext, mReminder);
        Reminder reminderFromService = mReminderService.getReminder(mId);
//        mReminderService.getAllNotifications();
        log1.log("testend");
        Thread.sleep(mWait+7000); // Allow Realm to sync.
        Reminder reminderRefetched = mReminderService.getReminder(mId);

        assertTrue(reminderRefetched.isTriggered());

//        R.id.notifications_menu_delete:
//        selectionCount = mAdapter.selectionCount(mRecycler);
//        if(selectionCount < 1) return true;
//
//        mAdapter.deleteSelected(mRecycler);
//        mAdapter.reload(null);
//        mode.finish();
//
//        if(selectionCount > 1) showSnackbar(mCoordinator, getString(R.string.notifications_deleted));
//        else showSnackbar(mCoordinator, getString(R.string.notification_deleted));
//        return true;
//        mReminderService.registerReminder(mContext, mReminder);
//        Reminder reminderFromService = mReminderService.getReminder(mId);
//
//        assertNotNull(reminderFromService);
//        assertTrue(reminderFromService.id().equals(mReminder.id()));
    }

}
