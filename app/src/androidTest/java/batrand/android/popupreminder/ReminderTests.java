package batrand.android.popupreminder;

import android.content.Context;
import androidx.test.InstrumentationRegistry;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import batrand.android.popupreminder.models.reminder.Reminder;
import batrand.android.popupreminder.services.alarm.AlarmService;
import batrand.android.popupreminder.services.alarm.IAlarmService;
import batrand.android.popupreminder.services.formatter.FormatService;
import batrand.android.popupreminder.services.log.Log;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.services.reminder.ReminderService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * Created by batra on 2017-05-09.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ReminderTests {

    private int mWait = 3; // Seconds to wait
    private long secondsFromNow(int seconds) { return System.currentTimeMillis() + (seconds*1000); }
    private String mId;
    private Reminder mReminder;

    private IReminderService mReminderService;
    private IAlarmService mAlarmService;
    private IPopupService mPopupService;
    private Context mContext;

    @Before
    public void setup() {
        mAlarmService = new AlarmService();
        mPopupService = mock(IPopupService.class);
        mReminderService = new ReminderService(mAlarmService, mPopupService, new Log(), new FormatService());

        mContext = InstrumentationRegistry.getTargetContext();

        mReminder = new Reminder();
        mId = mReminder.id();
        mReminder.setAsTrigger(secondsFromNow(mWait));
    }

    @After
    public void cleanup() {
        mReminderService.unregisterReminder(mContext, mId);
        mAlarmService.cancelAlarm(mContext, mId);
    }

    @Test
    public void setReminderIsPersisted() {
        mReminderService.registerReminder(mContext, mReminder);
        Reminder reminderFromService = mReminderService.getReminder(mId);

        assertNotNull(reminderFromService);
        assertTrue(reminderFromService.id().equals(mReminder.id()));
    }

    @Test
    public void settingReminderAlsoSetsAlarm() {
        mReminderService.registerReminder(mContext, mReminder);
        assertTrue(mAlarmService.isAlarmSet(mContext, mId));
    }

    @Test
    public void canUnregisterReminder() {
        mReminderService.registerReminder(mContext, mReminder);
        Reminder reminder = mReminderService.getReminder(mId);
        assertNotNull(reminder);

        mReminderService.unregisterReminder(mContext, mReminder.id());
        Reminder reminderTwo = mReminderService.getReminder(mId);
        assertNull(reminderTwo);
    }

    @Test
    public void unregisteringReminderAlsoCancelsAlarm() {
        mReminderService.registerReminder(mContext, mReminder);
        assertTrue(mAlarmService.isAlarmSet(mContext, mId));

        mReminderService.unregisterReminder(mContext, mId);
        assertFalse(mAlarmService.isAlarmSet(mContext, mId));
    }

    @Test
    public void reminderTriggersOnTime() throws InterruptedException {
        mReminderService.registerReminder(mContext, mReminder);
        Reminder reminder = mReminderService.getReminder(mId);
        assertFalse(reminder.isTriggered());
        //This time is needed for sync, without it getReminder will return null
        Thread.sleep(mWait+7000); // Allow Realm to sync.

        Reminder reminderRefetched = mReminderService.getReminder(mId);
        assertTrue(reminderRefetched.isTriggered());
    }

    @Test
    public void invalidReminderReturnsFalse() {
        mReminder.setAsTrigger(System.currentTimeMillis() - 900000);
        assertFalse("Invalid reminder does not report as invalid.",mReminder.isValidReminder(mReminderService.getConfigs()));
        boolean result = mReminderService.registerReminder(mContext, mReminder);
        assertFalse("Registering invalid reminder was allowed to succeed",result);
    }
}
