package batrand.android.popupreminder;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import batrand.android.popupreminder.models.reminder.Reminder;
import batrand.android.popupreminder.services.alarm.AlarmService;
import batrand.android.popupreminder.services.alarm.IAlarmService;
import batrand.android.popupreminder.services.formatter.FormatService;
import batrand.android.popupreminder.services.log.Log;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.popup.PopupService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.services.reminder.ReminderService;
import batrand.android.popupreminder.views.ReminderDetailActivity;
import static androidx.test.espresso.action.ViewActions.click;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

//import android.util.Log;

/**
 * Created by batra on 2017-05-09.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ReminderNotificationsTests {

    private int mWait = 3; // Seconds to wait
    private long secondsFromNow(int seconds) { return System.currentTimeMillis() + (seconds*1000); }
    private String mId;
    private Reminder mReminder;
    ReminderDetailActivity rda;
    private IReminderService mReminderService;
    private IAlarmService mAlarmService;
    private IPopupService mPopupService;
    private Context mContext;

    @Before
    public void setup() {
        mAlarmService = new AlarmService();
        mPopupService = new PopupService();

//        mReminderService = new ReminderService(mAlarmService, mPopupService, new Log(), new FormatService());

        mContext = InstrumentationRegistry.getTargetContext();

        mReminder = new Reminder();
        mId = mReminder.id();
        mReminder.setAsTrigger(secondsFromNow(mWait));
    }

    @After
    public void cleanup() {
//        mReminderService.unregisterReminder(mContext, mId);
        mAlarmService.cancelAlarm(mContext, mId);
    }


    @Test
    public void showNotificationsList() throws InterruptedException {
        Log log1=new Log();
        log1.log("test");
        Context contextMy=InstrumentationRegistry.getTargetContext();

mPopupService.popup(contextMy,"Test");
//niech będzie ze wezmiemy counter
//contextMy.getApplicationContext().
//        R.id.expanded_view_open_app_btn
//        startActivity(new Intent("com.company.package.FOO"));

//        onView(withId(R.id.floating_view))        // withId(R.id.my_view) is a ViewMatcher
////        onView(withId(R.id.expanded_view_open_app_btn))        // withId(R.id.my_view) is a ViewMatcher
//                .perform(click())   ;            // click() is a ViewAction
//        R.drawable.main_bottom_button
//                .check(matches(isDisplayed()));
//        ViewInteraction vI;
//        vI.check()
//        mPopupService.ge
//        Thread.sleep(6000);
        log1.log("test");

    }

    @Test
    public void testMockIntent() {
//        Intent intent=new Intent();
//        intent.setAction(Ac)
//        setActivityIntent();
//        Activity foo = getActivity();

//        assertNotNull(foo); // your tests
    }

//    public void showNotificationsFromMainActivity() throws InterruptedException {
//        Log log1=new Log();
//        log1.log("test");
//        MainActivity mainActivity=new MainActivity();
////        mainActivity.onCrea
//        onView(withId(R.id.floating_view))        // withId(R.id.my_view) is a ViewMatcher
////        onView(withId(R.id.expanded_view_open_app_btn))        // withId(R.id.my_view) is a ViewMatcher
//                .perform(click())   ;
////        mainActivity.startActivityFromFragment(Fragment);
//        log1.log("test");
//
//    }
}
