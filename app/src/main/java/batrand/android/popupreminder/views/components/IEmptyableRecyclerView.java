package batrand.android.popupreminder.views.components;

import android.view.View;

/**
 * Created by batra on 2017-05-26.
 */

public interface IEmptyableRecyclerView {
    void setEmptyView(View view);
}
