package batrand.android.popupreminder.views.bases;

import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.snackbar.Snackbar;

/**
 * Created by batra on 2017-05-23.
 */

public abstract class BaseActivity extends AppCompatActivity {
//    protected void showSnackbar(View coordinator, String message) {
//        Snackbar.make(coordinator, message, Snackbar.LENGTH_SHORT).show();
//    }

    protected void showSnackbarWithNullAction(View coordinator, String message, String action) {
        Snackbar bar = Snackbar.make(coordinator, message, Snackbar.LENGTH_INDEFINITE);
        bar.setAction(action, new View.OnClickListener() {
            @Override
            public void onClick(View v) {}
        });
        bar.show();
    }

//    protected void showSnackbarWithAction(View coordinator, String message, String action, View.OnClickListener actionListener) {
//        Snackbar bar = Snackbar.make(coordinator, message, Snackbar.LENGTH_INDEFINITE);
//        bar.setAction(action, actionListener);
//        bar.show();
//    }
}
