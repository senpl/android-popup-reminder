package batrand.android.popupreminder.views;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.services.popup.IPopupSoundService;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;
import batrand.android.popupreminder.services.DatabaseExporter;
/**
 * Created by batra on 2017-07-04.
 */

public class SettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    public static class SettingsFragment extends PreferenceFragment {
        @Inject IPopupSoundService mPopupSound;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);

            getInjector().inject(this);

            Preference playSoundPref = findPreference(getString(R.string.prefkey_pref_play_popup_sound));
            playSoundPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    mPopupSound.playSound(getActivity());
                    return true;
                }
            });

//            Preference exportDatabase = findPreference(getString(R.string.prefkey_pref_play_popup_sound));
//            exportDatabase.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//                @Override
//                public boolean onPreferenceClick(Preference preference) {
//                    DatabaseExporter.exportDatabase();
//                    return true;
//                }
//            });
//            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(mChangeListener);
            Preference showInternetRationalePref = findPreference(getString(R.string.prefkey_pref_show_internet_permission_rationale));
            showInternetRationalePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    startActivity(new Intent(getActivity(), InternetRationaleActivity.class));
                    return true;
                }
            });

            Preference shouldReportCrashesPref = findPreference(getString(R.string.prefkey_pref_should_report_crashes));
            shouldReportCrashesPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(R.string.restart_to_apply_pref)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {}
                            })
                            .create().show();
                    return true;
                }
            });

            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(mChangeListener);
        }

        @Override
        public void onResume() {
            super.onResume();
            refreshPrefSummaries();
        }

        private SharedPreferences.OnSharedPreferenceChangeListener mChangeListener
                = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                refreshPrefSummary(key);
            }
        };

        private void refreshPrefSummary(String prefKey) {
            Preference pref = findPreference(prefKey);

            if(pref instanceof ListPreference)
                pref.setSummary(((ListPreference) pref).getEntry());
            if(pref instanceof EditTextPreference)
                pref.setSummary(((EditTextPreference) pref).getText());
        }

        /**
         * Assuming that the PrefScreen has only child PrefCategs, and child PrefCategs
         * have only child Prefs and no further levels down.
         */
        private void refreshPrefSummaries() {
            // Looping over child PrefCategs
            for(int c = 0; c < getPreferenceScreen().getPreferenceCount(); c++) {
                Preference prefCateg = getPreferenceScreen().getPreference(c);

                // Looping over child Preferences
                if(prefCateg instanceof PreferenceCategory) {
                    for(int p = 0; p < ((PreferenceCategory) prefCateg).getPreferenceCount(); p++) {
                        refreshPrefSummary(((PreferenceCategory) prefCateg).getPreference(p).getKey());
                    }
                }
            }
        }
    }
}
