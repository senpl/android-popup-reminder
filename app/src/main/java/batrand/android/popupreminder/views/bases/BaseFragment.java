package batrand.android.popupreminder.views.bases;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;

import batrand.android.popupreminder.views.callbacks.IMainCabActivity;

/**
 * Created by batra on 2017-05-16.
 */

public abstract class BaseFragment extends Fragment {
    protected abstract int getLayoutId();

    protected void showSnackbar(View coordinator, String message) {
        Snackbar.make(coordinator, message, Snackbar.LENGTH_SHORT).show();
    }

    protected void showSnackbarWithNullAction(View coordinator, String message, String action) {
        Snackbar.make(coordinator, message, Snackbar.LENGTH_INDEFINITE)
                .setAction(action, null)
                .show();
    }

    protected void showSnackbarWithAction(View coordinator, String message, String action, View.OnClickListener actionListener) {
        Snackbar.make(coordinator, message, Snackbar.LENGTH_INDEFINITE)
                .setAction(action, actionListener)
                .show();
    }

    protected IMainCabActivity mCabHost;
    public void setCabHost(IMainCabActivity cabHost) { mCabHost = cabHost; }
    public IMainCabActivity getCabHost() { return mCabHost; }
}
