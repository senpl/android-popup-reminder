package batrand.android.popupreminder.views;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

import javax.inject.Inject;
import batrand.android.popupreminder.R;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.views.adapters.RemindersAdapter;
import batrand.android.popupreminder.views.bases.BaseFragment;
import batrand.android.popupreminder.views.callbacks.IMainCabActivity;
import batrand.android.popupreminder.views.callbacks.IOnRecyclerItemLongClick;
import batrand.android.popupreminder.views.components.EmptyableRecyclerView;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

public class RemindersFragment extends BaseFragment {
    @Override protected int getLayoutId() { return R.layout.fragment_reminders; }
    public RemindersFragment() {} // Required empty public constructor
    static RemindersFragment newInstance(IMainCabActivity cabHost) {
        RemindersFragment fragment = new RemindersFragment();
        fragment.setCabHost(cabHost);
        return fragment;
    }

    @Inject
    IPopupService mPopupService;

    @Inject IReminderService mReminderService;

    private CoordinatorLayout mCoordinator;

    private RemindersAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        getInjector().inject(this);

        view.findViewById(R.id.minimalize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupService.popup(getContext(), "any");
                LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(mReloadReceiver);
                requireActivity().finish();
            }
        });

        mCoordinator = view.findViewById(R.id.reminders_coordinator);
        FloatingActionButton mAddButton = view.findViewById(R.id.reminders_add_button);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(mReloadReceiver);
                startActivityForResult(
                        new Intent(getActivity(), ReminderDetailActivity.class), ReminderDetailActivity.ADD_REQUEST
                );
            }
        });

        EmptyableRecyclerView mRecycler = view.findViewById(R.id.reminders_recycler);


        mAdapter = new RemindersAdapter(this, mItemLongClickHandler);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        ItemTouchHelper touchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT){
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder dragged, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                mAdapter.deleteSelected(position);
                mAdapter.reload(null);
           }
        });
        touchHelper.attachToRecyclerView(mRecycler);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ReminderDetailActivity.ADD_REQUEST && resultCode == Activity.RESULT_OK) {
            mAdapter.reload(null);
            showSnackbar(mCoordinator, getString(R.string.reminder_added));
        }
        if(requestCode == ReminderDetailActivity.EDIT_REQUEST && resultCode == Activity.RESULT_OK) {
            mAdapter.reload(null);
            showSnackbar(mCoordinator, getString(R.string.reminder_edited));
        }
    }

    private final BroadcastReceiver mReloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!Objects.equals(intent.getAction(), mReminderService.getConfigs().getChangeAvailableBroadcastString()))
                return;
            mAdapter.reload(null);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        mPopupService.popup(getContext(),"any");
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(mReloadReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(requireActivity())
                .registerReceiver(mReloadReceiver,
                        new IntentFilter(mReminderService.getConfigs().getChangeAvailableBroadcastString())
                );
        checkIfNotificationIsBlocked();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPopupService.popup(getContext(),"any");
        LocalBroadcastManager.getInstance(requireActivity())
                .registerReceiver(mReloadReceiver,
                        new IntentFilter(mReminderService.getConfigs().getChangeAvailableBroadcastString())
                );
    }

    private final IOnRecyclerItemLongClick mItemLongClickHandler = new IOnRecyclerItemLongClick() {
        @Override
        public void onItemLongClick(int itemPosition) {
        }
    };

    /**
     * If the default notification channel (Android O+) is blocked, the
     * foreground services may have troubles. Display a warning and a
     * button to the Notification settings page.
     */
    private void checkIfNotificationIsBlocked() {
        if(getContext() == null) return;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = requireActivity().getSystemService(NotificationManager.class);
            assert notificationManager != null;
            NotificationChannel channel = notificationManager.getNotificationChannel(getString(R.string.notification_channel_id));
            if(channel.getImportance() == NotificationManager.IMPORTANCE_NONE) {
                showSnackbarWithAction(mCoordinator,
                        getString(R.string.notification_channel_disabled),
                        getString(R.string.unblock),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
                                i.putExtra(Settings.EXTRA_APP_PACKAGE, getString(R.string.app_package));
                                startActivity(i);
                            }
                        });
            }
        }
    }
}
