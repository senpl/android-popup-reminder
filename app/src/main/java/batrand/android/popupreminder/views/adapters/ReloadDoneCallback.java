package batrand.android.popupreminder.views.adapters;

/**
 * Created by batra on 2017-05-24.
 */
public interface ReloadDoneCallback {
    void onReloadDone();
}
