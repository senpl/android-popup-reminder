package batrand.android.popupreminder.views;

import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.services.settings.ISettingsService;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

public class InternetRationaleActivity extends AppCompatActivity {

    @Inject ISettingsService mSettings;
    private TextView mRationaleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet_rationale);
        getInjector().inject(this);

        mSettings.setHasSeenInternetPermissionRationale(this, true);
        mRationaleTextView = findViewById(R.id.internet_rationale_textview);
        mRationaleTextView.setText(Html.fromHtml(getString(R.string.why_internet_permission_required)));
    }
}
