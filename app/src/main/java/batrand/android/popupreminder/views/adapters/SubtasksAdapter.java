package batrand.android.popupreminder.views.adapters;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.models.Subtask;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.views.callbacks.IOnRecyclerItemLongClick;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

/**
 * Created by batra on 2017-05-24.
 * @modified by senpl
 */

public class SubtasksAdapter extends RecyclerView.Adapter<SubtasksAdapter.ViewHolder> {

    public String remainderId;
    private List<Subtask> msubtasks= new ArrayList<>();
    private final Context mContext;
    private IOnRecyclerItemLongClick mItemLongClickHandler;
    @Inject IReminderService mService;
    @Inject IFormatService mFormatter;
    private final Handler mRetrievalHandler = new Handler();

    public SubtasksAdapter(Context context, @Nullable IOnRecyclerItemLongClick itemLongClick) {
        mContext = context;
        if(itemLongClick != null) mItemLongClickHandler = itemLongClick;
        getInjector().inject(this);
        reload(null);
    }

    public void reload(final ReloadDoneCallback callback) {
        mRetrievalHandler.post(new Runnable() {
            @Override
            public void run() {
                if(msubtasks!=null && msubtasks.size()>0 && msubtasks.get(0)!=null) {
                    setSubtasks(mService.getSubtasksForRemainder(remainderId));
                    notifyDataSetChanged();
                }else if(msubtasks==null){
                    setSubtasks(mService.getSubtasksForRemainder(remainderId));
                    notifyDataSetChanged();
                }
                if(callback!=null) callback.onReloadDone();
            }
        });
    }

//    public void reloadNextAction(final ReloadDoneCallback callback) {
//        mRetrievalHandler.post(new Runnable() {
//            @Override
//            public void run() {
//                setSubtasks(mService.getSubtasksForRemainder(remainderId));
//                notifyDataSetChanged();
//                if (callback != null) callback.onReloadDone();
//            }
//        });
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View notificationView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_subtask, parent, false);
        return new ViewHolder(notificationView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Subtask notification = getSubtasks().get(position);
        holder.setSubtask(notification);
//        holder.setDeleteSeen();
    }

    @Override
    public int getItemCount() {
        if(getSubtasks() == null) return 0;
        return getSubtasks().size();
    }

    public List<Subtask> getSubtasks() {
        return msubtasks;
    }

    public void setSubtasks(List<Subtask> msubtasks) {
        this.msubtasks = msubtasks;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTimestampTextView;
        private TextView mTitleTextView;
        private TextView mDescriptionTextView;
        private Button mMarkSeenButton;
        private final AppCompatCheckBox mSelectionCheckbox;

        private Subtask subtask;

        ViewHolder(View itemView) {
            super(itemView);

            mTitleTextView = itemView.findViewById(R.id.subtask_title);
//            mDescriptionTextView = itemView.findViewById(R.id.notification_description);
            mTimestampTextView = itemView.findViewById(R.id.subtask_timestamp);
            mMarkSeenButton = itemView.findViewById(R.id.subtask_mark_seen_button);
            mSelectionCheckbox = itemView.findViewById(R.id.subtask_selection_checkbox);

            if(mItemLongClickHandler != null) {
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if(mItemLongClickHandler == null) return false;

                        mItemLongClickHandler.onItemLongClick(getAdapterPosition());
                        setSelected(true);
                        return true;
                    }
                });
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mSelectionCheckbox.getVisibility() == View.VISIBLE)
                        setSelected(!mSelectionCheckbox.isChecked());
                }
            });
        }

        void setSubtask(Subtask notification) {
            subtask = notification;
            String notificationText= subtask.getTitle();

            mTitleTextView.setText(notificationText);

            mMarkSeenButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if(mNotification.isSeen()) return;

//                    mService.remveSubtask(mContext, subtask.getTitle());
                    mMarkSeenButton.setEnabled(false);
                    mMarkSeenButton.setText(mContext.getString(R.string.checkmark));
                }
            });
//            mMarkSeenButton.setEnabled(!notification.isSeen());
//            if(!notification.isSeen())
//                mMarkSeenButton.setText(mContext.getString(R.string.mark_seen));
//            else mMarkSeenButton.setText(mContext.getString(R.string.checkmark));
//            mTimestampTextView.setText(mFormatter.formatDateTime(mContext, notification.timestamp()));
//
//            mMarkSeenButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(subtask.isSeen()) return;
//
//                    mService.markNotificationSeen(mContext, subtask.id());
//                    mMarkSeenButton.setEnabled(false);
//                    mMarkSeenButton.setText(mContext.getString(R.string.checkmark));
//                }
//            });
        }

//        public void setDeleteSeen() {
//            mDeleteSeenButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Log log;
////                    mService.deleteNotification();
//                    //if(mNotification.isSeen()) return;
//
////                    mService.markNotificationSeen(mContext, mNotification.id());
////                    mMarkSeenButton.setEnabled(false);
////                    mMarkSeenButton.setText(mContext.getString(R.string.checkmark));
//                }
//            });
//        }
//
//        void setSelectionVisibility(boolean isCheckboxVisible) {
//            if(isCheckboxVisible) mSelectionCheckbox.setVisibility(View.VISIBLE);
//            else mSelectionCheckbox.setVisibility(View.GONE);
//        }


        void setSelected(boolean isSelected) {
            mSelectionCheckbox.setChecked(isSelected);
        }

//        boolean isSelected() { return mSelectionCheckbox.isChecked(); }
    }
}
