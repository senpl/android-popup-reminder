package batrand.android.popupreminder.views.components;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by batra on 2017-05-26.
 */

public abstract class BaseEmptyableRecyclerView extends RecyclerView implements IEmptyableRecyclerView {
    public BaseEmptyableRecyclerView(Context context) {
        super(context);
    }

    public BaseEmptyableRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseEmptyableRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
