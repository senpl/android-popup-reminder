package batrand.android.popupreminder.views;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.maltaisn.recurpicker.Recurrence;
import com.maltaisn.recurpicker.RecurrenceFormat;
import com.maltaisn.recurpicker.RecurrencePickerDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.models.Subtask;
import batrand.android.popupreminder.models.reminder.Reminder;
import batrand.android.popupreminder.models.reminder.ReminderType;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.services.settings.ISettingsService;
import batrand.android.popupreminder.views.adapters.SubtasksAdapter;
import batrand.android.popupreminder.views.bases.BaseActivity;
import batrand.android.popupreminder.views.callbacks.IOnRecyclerItemLongClick;
import batrand.android.popupreminder.views.components.EmptyableRecyclerView;
import io.realm.Realm;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;
import java.text.SimpleDateFormat;
import java.util.UUID;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;


/**
 * For adding or editing reminders
 */
public class ReminderDetailActivity extends BaseActivity implements RecurrencePickerDialog.RecurrenceSelectedCallback{
    private Recurrence recurrence;
    public static final int ADD_REQUEST = 25487;
    public static final int EDIT_REQUEST = 35847;
    /**
     * Use this as key for Edit Mode. Its value should be the ID
     * of the reminder to be edited.
     */
    public static final String EDIT_MODE_KEY = "EDIT_MODE";
    @Inject
    IPopupService mPopupService;
    @Inject IFormatService mFormatter;
    @Inject IReminderService mReminderService;
    @Inject ISettingsService mSettings;
    @Inject IReminderService mService;

    private CoordinatorLayout mCoordinator;
    private SubtasksAdapter subtasksAdapter;
    private String mRemainderMainId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_detail);
        getInjector().inject(this);

        // Create date formats
        final DateFormat dateFormatLong = new SimpleDateFormat("EEE MMM dd, yyyy", Locale.ENGLISH);  // Sun Dec 31, 2017
        DateFormat dateFormatShort = new SimpleDateFormat("dd-MM-yyyy", Locale.GERMANY);  // 31-12-2017

        final RecurrencePickerDialog picker = new RecurrencePickerDialog();
        picker.setDateFormat(dateFormatShort, dateFormatLong);

        mCoordinator = findViewById(R.id.addreminder_coordinator);
        mTitleField = findViewById(R.id.reminderTitleField);
        mDescriptionField = findViewById(R.id.reminderDescriptionField);
        mContextField = findViewById(R.id.reminderContextField);
        mDateField = findViewById(R.id.triggerDateField);
        mTimeField = findViewById(R.id.triggerTimeField);
        mTitleField = findViewById(R.id.reminderTitleField);

        mRecurrenceRepeatField = findViewById(R.id.recurrence_show_repeat);

        mHideTimeField = findViewById(R.id.hide_timeTimeField);
        mHideBeforeTimeField = findViewById(R.id.hide_before_timeTimeField);
        mIsIntervalCheckbox = findViewById(R.id.reminder_is_interval_checkbox);

        findViewById(R.id.addreminder_donebutton).setOnClickListener(mAddButtonListener);
        findViewById(R.id.triggerDateFieldListener).setOnClickListener(mDateFieldListener);
        findViewById(R.id.triggerTimeFieldListener).setOnClickListener(mTimeFieldListener);
        findViewById(R.id.hide_timeTimeFieldListener).setOnClickListener(mHideTimeFieldListener);
        findViewById(R.id.hide_before_timeTimeFieldListener).setOnClickListener(mHideBeforeTimeFieldListener);

        ArrayAdapter<CharSequence> intervalTypeAdapter = ArrayAdapter.createFromResource(this, R.array.interval_types, android.R.layout.simple_spinner_item);
        intervalTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        recurrenceFormat=new RecurrenceFormat(getBaseContext(), dateFormatShort);

        EmptyableRecyclerView mSubtaskRecycler = findViewById(R.id.subtasks_recycler);
        subtasksAdapter =new SubtasksAdapter(this, mItemLongClickHandler);
        mSubtaskRecycler.setAdapter(subtasksAdapter);
        mSubtaskRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        subtasksAdapter = new NotificationsAdapter(this, mItemLongClickHandler);
//        mSubtaskRecycler.setAdapter(subtasksAdapter);
//        mSubtaskRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        prefillFields();

        if(getIntent().getExtras()!=null) {
            String editReminderId = getIntent().getExtras().getString(EDIT_MODE_KEY);
            if(editReminderId == null) return;

            // Is in edit mode

            mEditReminder = mReminderService.getReminder(editReminderId);
            setTitle(R.string.title_edit_reminder);

            populateEditFields();
        }

        Button btn = findViewById(R.id.recurrence_picker_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Open recurrence picker dialog
                picker.setRecurrence(recurrence, mTimeSelection.getTimeInMillis());
                picker.show(getSupportFragmentManager(), "recur_picker");
            }
        });



    }

    //region Edit mode
    private Reminder mEditReminder;

    /**
     * Populate fields with information from the reminder being edited
     */
    private void populateEditFields() {
        mTitleField.setText(mEditReminder.getTitle());
        mDescriptionField.setText(mEditReminder.getDescription());
        mContextField.setText(mEditReminder.getTaskContext());

        if(mEditReminder.getReminderType() == ReminderType.INTERVAL) {
            recurrence=new Recurrence(mEditReminder.getRecurrenceData(),0);
            mIsIntervalCheckbox.setChecked(true);
        }

        // Populate time fields
        long triggerTime;
        if(mEditReminder.getReminderType() == ReminderType.TRIGGER)
            triggerTime = mEditReminder.getTriggerTime();
        else triggerTime = mEditReminder.getNextAlarmMillis(null);

        setTimeFields(triggerTime);

        if(mEditReminder.getHideHour()!=0 || mEditReminder.getHideMinute()!=0) {
            mHideTimeSet = true;
            mHideTimeSelection = Calendar.getInstance();
            mHideTimeSelection.setTime(
                    new GregorianCalendar(0, 0, 0, mEditReminder.getHideHour(), mEditReminder.getHideMinute()).getTime()
            );
            mHideTimeField.setText(mFormatter.formatTime24(mEditReminder.getHideHour(), mEditReminder.getHideMinute()));
        }

        if(mEditReminder.getHideBeforeHour()!=0 || mEditReminder.getHideBeforeMinute()!=0) {
            mHideBeforeTimeSet = true;
            mHideBeforeTimeSelection = Calendar.getInstance();
            mHideBeforeTimeSelection.setTime(
                    new GregorianCalendar(0, 0, 0, mEditReminder.getHideBeforeHour(), mEditReminder.getHideBeforeMinute()).getTime()
            );
            mHideBeforeTimeField.setText(mFormatter.formatTime24(mEditReminder.getHideBeforeHour(), mEditReminder.getHideBeforeMinute()));
        }

        LinearLayout subTasksLayout= findViewById(R.id.subtask_layout);
        subTasksLayout.setVisibility(View.VISIBLE);
        Button subTaskAddBtn = findViewById(R.id.subtask_add);
        subtasksAdapter.setSubtasks(mService.getSubtasksForRemainder(mEditReminder.id()));
        subtasksAdapter.reload(null);
        subTaskAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Subtask> toPersistSubtasks = subtasksAdapter.getSubtasks();
                Subtask subtaskAdded=new Subtask(mRemainderMainId);
                EditText subtaskTitle=findViewById(R.id.subtaskTextToAdd);
                subtaskAdded.setTitle(subtaskTitle.getText().toString());
                toPersistSubtasks.add(subtaskAdded);

                // Persist subtask
                Realm realm = Realm.getDefaultInstance();
                final List<Subtask> toPersistSubtasksFinal=toPersistSubtasks;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm tRealm) {
                        tRealm.copyToRealmOrUpdate(toPersistSubtasksFinal);
                    }
                });
                realm.close();
                subtasksAdapter.notifyItemInserted(1);
            }
        });
        subtasksAdapter.notifyDataSetChanged();
    }
    //endregion

    //region recurrence new
    private TextView mRecurrenceRepeatField;
    RecurrenceFormat recurrenceFormat;

    @Override
    public void onRecurrencePickerSelected(Recurrence r) {
        recurrence = r;
        mRecurrenceRepeatField.setText(recurrenceFormat.format(recurrence));
        if(recurrence.getPeriod() != Recurrence.NONE){
            mIsIntervalCheckbox.setChecked(true);
        }
    }
    //endregion

    @Override
    public void onRecurrencePickerCancelled(Recurrence r) {
        Toast.makeText(this, "Picker dialog cancelled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPopupService.popup(this,"any");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPopupService.popup(this,"any");
    }

    private final IOnRecyclerItemLongClick mItemLongClickHandler =
            new IOnRecyclerItemLongClick() {
                @Override
                public void onItemLongClick(int itemPosition) {
                }
            };

    //region Fields
    private final View.OnClickListener mAddButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Validation
            if(isTitleEmpty()) {
                showSnackbarWithNullAction(mCoordinator, getString(R.string.error_title_empty), getString(R.string.ok));
                return;
            }
            if(!mDateSet || !mTimeSet) {
                showSnackbarWithNullAction(mCoordinator, getString(R.string.error_trigger_time_not_set), getString(R.string.ok));
                return;
            }
            if(!isTriggerTimeInFuture()) {
                showSnackbarWithNullAction(mCoordinator, getString(R.string.error_trigger_time_past), getString(R.string.ok));
                return;
            }

            // Construct reminder
            Reminder reminder = new Reminder();
            if(mEditReminder != null) reminder = mEditReminder;
            reminder.setTitle(title());
            if(!isDescriptionEmpty()) {
                reminder.setDescription(description());
            }
            if (!isTaskContextEmpty()) {
                reminder.setTaskContext(taskContext());
            }
            if(isInterval()){
                //writing mTimeSelection into recurrence object
                recurrence.setStartDate(mTimeSelection.getTimeInMillis());
                reminder.setReminderType(ReminderType.INTERVAL);
                reminder.setRecurrenceData(recurrence.toByteArray());
            }
            else reminder.setAsTrigger(triggerTime());


            if (mHideTimeSelection != null ) {
                reminder.setHideHour(mHideTimeSelection.get(Calendar.HOUR_OF_DAY));
                reminder.setHideMinute(mHideTimeSelection.get(Calendar.MINUTE));
            }
            if(!reminder.isValidReminder(mReminderService.getConfigs())) {
                // All validation done above. Any errors should be informed to the user above.
                // This only prevents adding invalid Reminders to the database
                return;
            }

            mReminderService.registerReminder(ReminderDetailActivity.this, reminder);
            setResult(RESULT_OK);
            if(getIntent().getExtras()!=null) {
                String editReminderId = getIntent().getExtras().getString(EDIT_MODE_KEY);
                if (editReminderId == null) return;
                finishActivity(ReminderDetailActivity.EDIT_REQUEST);
            } else {
                finishActivity(ReminderDetailActivity.ADD_REQUEST);
            }
            finish();
        }
    };

    private EditText mTitleField;
    private EditText mDescriptionField;
    private EditText mContextField;

    private String title() { return mTitleField.getText().toString(); }

    private String description() { return mDescriptionField.getText().toString(); }

    private String taskContext() {
        return mContextField.getText().toString();
    }

    private boolean isTitleEmpty() { return title().length() == 0; }

    private boolean isDescriptionEmpty() { return description().length() == 0; }

    private boolean isTaskContextEmpty() {
        return taskContext().length() == 0;
    }

    private AppCompatCheckBox mIsIntervalCheckbox;

    private boolean isInterval() { return mIsIntervalCheckbox.isChecked(); }

    /**
     * Set Time and Date fields to this UNIX timestamp
     */
    private void setTimeFields(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        mTimeSelection = calendar;
        mDateSelection = calendar;
        updateTimeField();
        updateDateField();
    }

    private void prefillFields() {
        if(mSettings.shouldPrefillWithCurrentTime(this)) {
            final int twoMinutes = 120000;
            setTimeFields(System.currentTimeMillis() + twoMinutes);
        }
    }
    //endregion

    //region Date Time picking
    private TextView mDateField;
    private boolean mDateSet = false;
    private TextView mTimeField;
    private boolean mTimeSet = false;
    private Calendar mDateSelection;
    private Calendar mTimeSelection;

    private final View.OnClickListener mDateFieldListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar now = Calendar.getInstance();
            if(mDateSet) now.setTime(mDateSelection.getTime());

            DatePickerDialog.newInstance(mDateSetListener,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            ).show(getFragmentManager(), "DatePicker");
        }
    };


    private TextView mHideBeforeTimeField;
    private boolean mHideBeforeTimeSet = false;
    private Calendar mHideBeforeTimeSelection;

    private final View.OnClickListener mHideBeforeTimeFieldListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar now = Calendar.getInstance();
            if (mHideBeforeTimeSet) now.setTime(mHideBeforeTimeSelection.getTime());

            TimePickerDialog.newInstance(mHideBeforeTimeSetListener,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    now.get(Calendar.SECOND),
                    true
            ).show(getFragmentManager(), "TimePicker");
        }
    };

    private TextView mHideTimeField;
    private boolean mHideTimeSet = false;
    private Calendar mHideTimeSelection;

    private final View.OnClickListener mTimeFieldListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar now = Calendar.getInstance();
            if(mTimeSet) now.setTime(mTimeSelection.getTime());

            TimePickerDialog.newInstance(mTimeSetListener,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    now.get(Calendar.SECOND),
                    true
            ).show(getFragmentManager(), "TimePicker");
        }
    };

    private final View.OnClickListener mHideTimeFieldListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar now = Calendar.getInstance();
            if (mHideTimeSet) now.setTime(mHideTimeSelection.getTime());

            TimePickerDialog.newInstance(mHideTimeSetListener,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    now.get(Calendar.SECOND),
                    true
            ).show(getFragmentManager(), "TimePicker");
        }
    };

    private final TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
            mTimeSelection = Calendar.getInstance();
            mTimeSelection.setTime(new GregorianCalendar(0,0,0, hourOfDay, minute, 0).getTime());
            updateTimeField();
        }
    };

    private final TimePickerDialog.OnTimeSetListener mHideTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
            mHideTimeSelection = Calendar.getInstance();
            mHideTimeSelection.setTime(new GregorianCalendar(0, 0, 0, hourOfDay, minute, 0).getTime());
            updateHideTimeField();
        }
    };

    private final TimePickerDialog.OnTimeSetListener mHideBeforeTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
            mHideBeforeTimeSelection = Calendar.getInstance();
            mHideBeforeTimeSelection.setTime(new GregorianCalendar(0, 0, 0, hourOfDay, minute, 0).getTime());
            updateHideBeforeTimeField();
        }
    };

    private void updateTimeField() {
        mTimeSet = true;
        int hourOfDay = mTimeSelection.get(Calendar.HOUR_OF_DAY);
        int minute = mTimeSelection.get(Calendar.MINUTE);
        mTimeField.setText(mFormatter.formatTime24(hourOfDay, minute));
    }

    private void updateHideTimeField() {
        mHideTimeSet = true;
        int hourOfDay = mHideTimeSelection.get(Calendar.HOUR_OF_DAY);
        int minute = mHideTimeSelection.get(Calendar.MINUTE);
        mHideTimeField.setText(mFormatter.formatTime24(hourOfDay, minute));
    }

    private void updateHideBeforeTimeField() {
        mHideBeforeTimeSet = true;
        int hourOfDay = mHideBeforeTimeSelection.get(Calendar.HOUR_OF_DAY);
        int minute = mHideBeforeTimeSelection.get(Calendar.MINUTE);
        mHideBeforeTimeField.setText(mFormatter.formatTime24(hourOfDay, minute));
    }

    private final DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            mDateSelection = Calendar.getInstance();
            mDateSelection.setTime(new GregorianCalendar(year, monthOfYear, dayOfMonth, 0,0,0).getTime());
            updateDateField();
        }
    };

    private void updateDateField() {
        mDateSet = true;
        int year = mDateSelection.get(Calendar.YEAR);
        int monthOfYear = mDateSelection.get(Calendar.MONTH);
        int dayOfMonth = mDateSelection.get(Calendar.DAY_OF_MONTH);
        mDateField.setText(mFormatter.formatDate(year, monthOfYear, dayOfMonth));
    }

    private long triggerTime() {
        Calendar triggerTime = Calendar.getInstance();
        triggerTime.set(
                mDateSelection.get(Calendar.YEAR),
                mDateSelection.get(Calendar.MONTH),
                mDateSelection.get(Calendar.DAY_OF_MONTH),
                mTimeSelection.get(Calendar.HOUR_OF_DAY),
                mTimeSelection.get(Calendar.MINUTE),
                mTimeSelection.get(Calendar.SECOND)
        );
        return triggerTime.getTimeInMillis();
    }

    private long triggerTimeFromRecurrence(Recurrence r) {
        recurrence=r;
        Calendar triggerTime = Calendar.getInstance();
        if(triggerTime()>System.currentTimeMillis())
            return triggerTime();
        else {
            List<Long> foundRecurrenceList = recurrence.findRecurrences(System.currentTimeMillis(), 1);
            long foundRecurrence = 0;
            if (!foundRecurrenceList.isEmpty())
                foundRecurrence = foundRecurrenceList.get(0);
            triggerTime.setTimeInMillis(foundRecurrence);
        }
        triggerTime.set(
                triggerTime.get(Calendar.YEAR),
                triggerTime.get(Calendar.MONTH),
                triggerTime.get(Calendar.DAY_OF_MONTH),
                mTimeSelection.get(Calendar.HOUR_OF_DAY),
                mTimeSelection.get(Calendar.MINUTE),
                mTimeSelection.get(Calendar.SECOND)
        );
        return triggerTime.getTimeInMillis();
    }

    private boolean isTriggerTimeInFuture() {
        if(isInterval()) {
            long time=triggerTimeFromRecurrence(recurrence);
            long millis=System.currentTimeMillis();
//            Date d=new Date();
            //Log.i("", "isTriggerTimeInFuture: "+d.toString());
            return time > millis;
        } else {
            return triggerTime() > System.currentTimeMillis();
        }
    }
    //endregion
}
