package batrand.android.popupreminder.views;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mikepenz.aboutlibraries.Libs;
import com.mikepenz.aboutlibraries.LibsBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.models.reminder.Reminder;
import batrand.android.popupreminder.services.DatabaseExporter;
import io.realm.Realm;

public class AboutActivity extends AppCompatActivity {

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        // Setting version info
        TextView appVersion = findViewById(R.id.app_version_textview);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionString = getString(R.string.app_version_formattable, pInfo.versionName, Integer.toString(pInfo.versionCode));
            appVersion.setText(versionString);
        } catch(PackageManager.NameNotFoundException e) {
            appVersion.setVisibility(View.GONE);
        }
//        findViewById(R.id.about_database_export).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(AboutActivity.this, ShowDatabaseToCopy.class));
//            }
//        });
//        findViewById(R.id.about_database_export).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(AboutActivity.this, ShowDatabaseToCopy.class));
//            }
//        });
//        findViewById(R.id.about_database_export).setOnClickListener(new View.OnClickListener() {
////            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//
//            @Override
//            public void onClick(View v) {
////                FileUtils.copyDb();`
//
//                Realm realm = Realm.getDefaultInstance();
//                File sd = Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
////                File file = new File(sd.getAbsolutePath() + "/defaultName.ralm");
//                Log.d("", "done export db");
////                String path = realm.getPath();
//                Gson gson = new Gson();//... obtain your Gson
//                Reminder managedReminder = realm.where(Reminder.class).findFirst();
//                if (managedReminder != null) {
//                    managedReminder = realm.copyFromRealm(managedReminder); //detach from Realm, copy values to fields
////                    String json = gson.toJson(managedReminder);
//                    try {
//                        File outputDb=new File(sd.getAbsolutePath() , "/output.json");
//                        if(!outputDb.exists()) {
//                            boolean created=outputDb.createNewFile();
//                        }
//                        try (Writer writer = new FileWriter(sd.getAbsolutePath() + "/output.json")) {
////                            Gson gson = new GsonBuilder().create();
//                            gson.toJson(managedReminder, writer);
//                            System.out.println("done");
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
////                    System.out.println(json);
//                }
//            }
//        });
        // Click listeners
//        findViewById(R.id.about_button_libs).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new LibsBuilder()
//                        .withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR)
//                        .withActivityTitle(getString(R.string.title_about_libs))
//                        .withAutoDetect(true)
//                        .withLicenseShown(true)
//                        .withLicenseDialog(true)
//                        .withLibraries("Dagger","MaterialDTPicker", "MaterialSeekbarPref")
//                        .start(AboutActivity.this);
//            }
//        });
        findViewById(R.id.about_button_audio).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutActivity.this, AboutSoundsActivity.class));
            }
        });
        findViewById(R.id.about_button_logs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutActivity.this, LogActivity.class));
            }
        });
        findViewById(R.id.about_button_sourcelink).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.app_sourcelink)));
                startActivity(intent);
            }
        });
    }
}
