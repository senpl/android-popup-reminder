package batrand.android.popupreminder.views.adapters;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.models.LogEntry;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.log.ILog;
import io.realm.Sort;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

/**
 * Created by batra on 2017-06-07.
 */

public class LogsAdapter extends RecyclerView.Adapter<LogsAdapter.ViewHolder> {

    private List<LogEntry> mLogEntries;
    private Context mContext;
    private Handler mRetrievalHandler = new Handler();

    @Inject ILog mService;
    @Inject IFormatService mFormatter;

    public LogsAdapter(Context context) {
        mContext = context;
        getInjector().inject(this);
        reload(null);
    }

    public void reload(final ReloadDoneCallback callback) {
        mRetrievalHandler.post(new Runnable() {
            @Override
            public void run() {
                mLogEntries = mService.getAllEntries();
                notifyDataSetChanged();
                if(callback!=null) callback.onReloadDone();
            }
        });
    }

    public void sort(final Sort sortOrder) {
        mRetrievalHandler.post(new Runnable() {
            @Override
            public void run() {
                mLogEntries = mService.getAllEntriesSortedByTimestamp(sortOrder);
                notifyDataSetChanged();
            }
        });
    }

    public void clear() {
        mRetrievalHandler.post(new Runnable() {
            @Override
            public void run() {
                mService.clearLogs();
                reload(null);
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_log_entry, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogEntry entry = mLogEntries.get(position);
        holder.setLogEntry(entry);
    }

    @Override
    public int getItemCount() {
        if(mLogEntries == null) return 0;
        else return mLogEntries.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private LogEntry mLogEntry;
        private TextView mMessageTextView;
        private TextView mTimestampTextView;

        ViewHolder(View itemView) {
            super(itemView);
            mMessageTextView = itemView.findViewById(R.id.log_message_textview);
            mTimestampTextView = itemView.findViewById(R.id.log_timestamp_textview);
        }

        void setLogEntry(LogEntry logEntry) {
            mLogEntry = logEntry;
            mMessageTextView.setText(logEntry.message);
            mTimestampTextView.setText(mFormatter.formatDateTimeCompact(mContext, logEntry.timestamp));
        }
    }
}
