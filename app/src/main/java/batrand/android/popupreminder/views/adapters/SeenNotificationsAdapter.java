package batrand.android.popupreminder.views.adapters;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.models.Notification;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.log.Log;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.views.callbacks.IOnRecyclerItemLongClick;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

/**
 * Created by batra on 2017-05-24.
 * @modified by senpl
 */

public class SeenNotificationsAdapter extends RecyclerView.Adapter<SeenNotificationsAdapter.ViewHolder> {

    private List<Notification> mNotifications;
    private final Context mContext;
    private IOnRecyclerItemLongClick mItemLongClickHandler;
    @Inject IReminderService mService;
    @Inject IFormatService mFormatter;
    private final Handler mRetrievalHandler = new Handler();

    public SeenNotificationsAdapter(Context context, @Nullable IOnRecyclerItemLongClick itemLongClick) {
        mContext = context;
        if(itemLongClick != null) mItemLongClickHandler = itemLongClick;
        getInjector().inject(this);
        reload(null);
    }

    public void reload(final ReloadDoneCallback callback) {
        mRetrievalHandler.post(new Runnable() {
            @Override
            public void run() {
                mNotifications = mService.getSeenNotifications();
                notifyDataSetChanged();
                if(callback!=null) callback.onReloadDone();
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View notificationView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_seen_notification, parent, false);
        return new ViewHolder(notificationView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Notification notification = mNotifications.get(position);
        holder.setNotification(notification);
//        holder.setDeleteSeen();
    }

    @Override
    public int getItemCount() {
        if(mNotifications == null) return 0;
        return mNotifications.size();
    }

    /**
     * @param isEntering true means entering CAB, false means exiting CAB
     */
    public void setCabMode(RecyclerView notificationsRecycler, final boolean isEntering) {
        executeOnEachHolder(notificationsRecycler, new IExecuteWithHolder() {
            @Override
            public void executeWithHolder(ViewHolder holder, int holderPosition) {
                holder.setSelectionVisibility(isEntering);

                // Reset all checkboxes, whether entering or exiting
                holder.setSelected(false);
            }
        });
    }

    public void selectAll(RecyclerView notificationsRecycler) {
        executeOnEachHolder(notificationsRecycler, new IExecuteWithHolder() {
            @Override
            public void executeWithHolder(ViewHolder holder, int holderPosition) {
                holder.setSelected(true);
            }
        });
    }

    public void markSeenSelected(RecyclerView notificationsRecycler) {
        executeOnEachHolder(notificationsRecycler, new IExecuteWithHolder() {
            @Override
            public void executeWithHolder(ViewHolder holder, int holderPosition) {
                if(holder.isSelected())
                    mService.markNotificationSeen(mContext, mNotifications.get(holderPosition).id());
            }
        });
    }

//    public void markUnSeenSelected(RecyclerView notificationsRecycler) {
//        executeOnEachHolder(notificationsRecycler, new IExecuteWithHolder() {
//            @Override
//            public void executeWithHolder(ViewHolder holder, int holderPosition) {
//                if(holder.isSelected())
//                    mService.markNotificationSeen(mContext, mNotifications.get(holderPosition).id());
//            }
//        });
//    }

    public void deleteSeen(){
//        we need  id's that are seen
        List<Notification> listOfNotifications = mService.getSeenNotifications();
        for (Notification notification : listOfNotifications) {
            mService.deleteNotification(mContext, notification.id());
        }
    }

    public void deleteSelected(RecyclerView notificationsRecycler) {
        executeOnEachHolder(notificationsRecycler, new IExecuteWithHolder() {
            @Override
            public void executeWithHolder(ViewHolder holder, int holderPosition) {
                if(holder.isSelected())
                    mService.deleteNotification(mContext, mNotifications.get(holderPosition).id());
            }
        });
    }

    public int selectionCount(RecyclerView notificationsRecycler) {
        int selectionCount = 0;
        for(int i = 0; i < mNotifications.size(); i++) {
            ViewHolder holder = (ViewHolder) notificationsRecycler.findViewHolderForAdapterPosition(i);
            if(holder != null) {
                if(holder.isSelected()) selectionCount++;
            }
        }
        return selectionCount;
    }

    /**
     * Run the code in the execution on all ViewHolders of all items
     */
    private void executeOnEachHolder(RecyclerView recyler, IExecuteWithHolder execution) {
        for(int i = 0; i < mNotifications.size(); i++) {

            ViewHolder holder = (ViewHolder) recyler.findViewHolderForAdapterPosition(i);
            if(holder != null) {
                execution.executeWithHolder(holder, i);
            }
        }
    }

    private interface IExecuteWithHolder {
        /**
         * @param holderPosition position of the ViewHolder in the adapter
         */
        void executeWithHolder(ViewHolder holder, int holderPosition);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTimestampTextView;
        private TextView mTitleTextView;
        private TextView mDescriptionTextView;
        private Button mMarkSeenButton;
        private Button mDeleteSeenButton;
        private final AppCompatCheckBox mSelectionCheckbox;

        private Notification mNotification;

        ViewHolder(View itemView) {
            super(itemView);

            mTitleTextView = itemView.findViewById(R.id.notification_title);
            mDescriptionTextView = itemView.findViewById(R.id.notification_description);
            mTimestampTextView = itemView.findViewById(R.id.notification_timestamp);
            mMarkSeenButton = itemView.findViewById(R.id.notification_mark_seen_button);
            mDeleteSeenButton = itemView.findViewById(R.id.notification_delete_seen_button);
            mSelectionCheckbox = itemView.findViewById(R.id.notification_selection_checkbox);

            if(mItemLongClickHandler != null) {
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if(mItemLongClickHandler == null) return false;

                        mItemLongClickHandler.onItemLongClick(getAdapterPosition());
                        setSelected(true);
                        return true;
                    }
                });
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mDeleteSeenButton.callOnClick()){
                        deleteSeen();
                    }
                    if(mSelectionCheckbox.getVisibility() == View.VISIBLE)
                        setSelected(!mSelectionCheckbox.isChecked());
                }
            });


        }

        void setNotification(Notification notification) {
            mNotification = notification;

            mTitleTextView.setText(mNotification.reminderTitle());
            mDescriptionTextView.setText(mNotification.reminderDescription());

//            mMarkSeenButton.setEnabled(!notification.isSeen());
//            if(!notification.isSeen())
//                mMarkSeenButton.setText(mContext.getString(R.string.mark_seen));
//            else mMarkSeenButton.setText(mContext.getString(R.string.checkmark));
//            mTimestampTextView.setText(mFormatter.formatDateTime(mContext, notification.timestamp()));
//
//            mMarkSeenButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(mNotification.isSeen()) return;
//
//                    mService.markNotificationSeen(mContext, mNotification.id());
//                    mMarkSeenButton.setEnabled(false);
//                    mMarkSeenButton.setText(mContext.getString(R.string.checkmark));
//                }
//            });
        }

        public void setDeleteSeen() {
            mDeleteSeenButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log log;
//                    mService.deleteNotification();
                    //if(mNotification.isSeen()) return;

//                    mService.markNotificationSeen(mContext, mNotification.id());
//                    mMarkSeenButton.setEnabled(false);
//                    mMarkSeenButton.setText(mContext.getString(R.string.checkmark));
                }
            });
        }

        void setSelectionVisibility(boolean isCheckboxVisible) {
            if(isCheckboxVisible) mSelectionCheckbox.setVisibility(View.VISIBLE);
            else mSelectionCheckbox.setVisibility(View.GONE);
        }


        void setSelected(boolean isSelected) {
            mSelectionCheckbox.setChecked(isSelected);
        }

        boolean isSelected() { return mSelectionCheckbox.isChecked(); }


    }
}
