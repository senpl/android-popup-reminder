package batrand.android.popupreminder.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import batrand.android.popupreminder.R;
import batrand.android.popupreminder.models.Notification;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.views.adapters.NotificationsAdapter;
import batrand.android.popupreminder.views.bases.BaseFragment;
import batrand.android.popupreminder.views.callbacks.IMainCabActivity;
import batrand.android.popupreminder.views.callbacks.IOnRecyclerItemLongClick;
import batrand.android.popupreminder.views.components.EmptyableRecyclerView;
import io.realm.Realm;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

public class NotificationsFragment extends BaseFragment {

    @Override protected int getLayoutId() { return R.layout.fragment_notifications; }

    public NotificationsFragment() {} // Required empty public constructor

    static NotificationsFragment newInstance(IMainCabActivity cabHost) {
        NotificationsFragment fragment = new NotificationsFragment();
        fragment.setCabHost(cabHost);
        return fragment;
    }

    @Inject
    IPopupService mPopupService;
    @Inject IReminderService mReminderService;
    @Inject IFormatService mFormatter;

    private NotificationsAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        getInjector().inject(this);

        view.findViewById(R.id.minimalize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupService.popup(getContext(),"any");
                LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(mReloadReceiver);
                requireActivity().finish();
            }
        });

//        CoordinatorLayout mCoordinator = view.findViewById(R.id.notifications_coordinator);

        EmptyableRecyclerView mRecycler = view.findViewById(R.id.notifications_recycler);

        mAdapter = new NotificationsAdapter(getActivity(), mItemLongClickHandler);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        mRecycler.setEmptyView(view.findViewById(R.id.notifications_recycler_empty));

//        mRefresh = view.findViewById(R.id.notifications_recycler_swipe);
//        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                mAdapter.reload(new ReloadDoneCallback() {
//                   @Override
//                    public void onReloadDone() {
//                       mRefresh.setRefreshing(false);
//                   }
//                });
//            }
//        });

        ItemTouchHelper touchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0){
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder dragged, @NonNull RecyclerView.ViewHolder target) {

                int fromPosition = dragged.getAdapterPosition();
                int toPosition = target.getAdapterPosition();
                List<Notification> listOfNotifications= mAdapter.getNotifications();
                if (fromPosition < toPosition) {
                    for (int i = fromPosition; i < toPosition; i++) {
                        Collections.swap(listOfNotifications, i, i + 1);
                        long order1 = listOfNotifications.get(i).getOrder();
                        long order2 = listOfNotifications.get(i + 1).getOrder();
                        listOfNotifications.get(i).setOrder(order2);
                        listOfNotifications.get(i + 1).setOrder(order1);
                    }
                }
                else {
                    for (int i = fromPosition; i > toPosition; i--) {
                        Collections.swap(listOfNotifications, i, i - 1);
                        long order1 = listOfNotifications.get(i).getOrder();
                        long order2 = listOfNotifications.get(i - 1).getOrder();
                        listOfNotifications.get(i).setOrder(order2);
                        listOfNotifications.get(i - 1).setOrder(order1);
                    }
                }
                mAdapter.notifyItemMoved(fromPosition,toPosition);
                final List<Notification> toPersistNotificationsList=listOfNotifications;
                // Persist notifications
                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm tRealm) {
                        tRealm.copyToRealmOrUpdate(toPersistNotificationsList);
                    }
                });
                realm.close();

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            }
        });
        touchHelper.attachToRecyclerView(mRecycler);

        return view;
    }

    private final BroadcastReceiver mReloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(!Objects.equals(intent.getAction(), mReminderService.getConfigs().getChangeAvailableBroadcastString()))
                return;
            mAdapter.reload(null);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        mPopupService.popup(getContext(),"any");
        if(getActivity()!=null){
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReloadReceiver);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(requireActivity())
                .registerReceiver(mReloadReceiver,
                        new IntentFilter(mReminderService.getConfigs().getChangeAvailableBroadcastString())
                );
    }

    @Override
    public void onStop() {
        super.onStop();
        mPopupService.popup(getContext(),"any");
        LocalBroadcastManager.getInstance(requireActivity())
                .registerReceiver(mReloadReceiver,
                        new IntentFilter(mReminderService.getConfigs().getChangeAvailableBroadcastString())
                );
    }

    private final IOnRecyclerItemLongClick mItemLongClickHandler =
            new IOnRecyclerItemLongClick() {
        @Override
        public void onItemLongClick(int itemPosition) {
        }
    };

//    private ActionMode.Callback mCabCallback = new ActionMode.Callback() {
//        @Override
//        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
//            mode.getMenuInflater().inflate(R.menu.notifications_menu, menu);
//            return true;
//        }
//
//        @Override
//        public boolean onPrepareActionMode(ActionMode mode, Menu menu) { return false; }
//
//        @Override
//        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
//            int selectionCount;
//            switch(item.getItemId()) {
//                case R.id.notifications_menu_selectall:
//                    mAdapter.selectAll(mRecycler);
//                    return true;
//                case R.id.notifications_menu_mark_seen:
//                    selectionCount = mAdapter.selectionCount(mRecycler);
//                    if(selectionCount < 1) return true;
//
//                    mAdapter.markSeenSelected(mRecycler);
//                    mAdapter.reload(null);
//                    mode.finish();
//
//                    if(selectionCount > 1) showSnackbar(mCoordinator, getString(R.string.notifications_marked_seen));
//                    else showSnackbar(mCoordinator, getString(R.string.notification_marked_seen));
//                    return true;
//                case R.id.notifications_menu_delete:
//                    selectionCount = mAdapter.selectionCount(mRecycler);
//                    if(selectionCount < 1) return true;
//
//                    mAdapter.deleteSelected(mRecycler);
//                    mAdapter.reload(null);
//                    mode.finish();
//
//                    if(selectionCount > 1) showSnackbar(mCoordinator, getString(R.string.notifications_deleted));
//                    else showSnackbar(mCoordinator, getString(R.string.notification_deleted));
//                    return true;
//                default: return false;
//            }
//        }
//
//        @Override
//        public void onDestroyActionMode(ActionMode mode) {
//            mAdapter.setCabMode(mRecycler, false);
//            getCabHost().onCabStopped();
//        }
//    };
}
