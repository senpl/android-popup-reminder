package batrand.android.popupreminder.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import androidx.appcompat.view.ActionMode;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.views.adapters.SeenNotificationsAdapter;
import batrand.android.popupreminder.views.adapters.ReloadDoneCallback;
import batrand.android.popupreminder.views.bases.BaseFragment;
import batrand.android.popupreminder.views.callbacks.IMainCabActivity;
import batrand.android.popupreminder.views.callbacks.IOnRecyclerItemLongClick;
import batrand.android.popupreminder.views.components.EmptyableRecyclerView;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

public class SeenFragment extends BaseFragment {

    @Override protected int getLayoutId() { return R.layout.fragment_seen_notifications; }

    public SeenFragment() {} // Required empty public constructor

    static SeenFragment newInstance(IMainCabActivity cabHost) {
        SeenFragment fragment = new SeenFragment();
        fragment.setCabHost(cabHost);
        return fragment;
    }

    @Inject IPopupService mPopupService;
    @Inject IReminderService mReminderService;
    @Inject IFormatService mFormatter;

    private CoordinatorLayout mCoordinator;
    private EmptyableRecyclerView mRecycler;
    private SeenNotificationsAdapter mAdapter;
    private SwipeRefreshLayout mRefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        getInjector().inject(this);

        view.findViewById(R.id.minimalize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupService.popup(getContext(), "any");
                LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(mReloadReceiver);
                requireActivity().finish();
            }
        });

        view.findViewById(R.id.notification_delete_seen_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.deleteSeen();
                mRefresh.setRefreshing(false);
            }
        });


        mCoordinator = view.findViewById(R.id.notifications_coordinator);

        mRecycler = view.findViewById(R.id.notifications_recycler);
        mAdapter = new SeenNotificationsAdapter(getActivity(), mItemLongClickHandler);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecycler.setEmptyView(view.findViewById(R.id.notifications_recycler_empty));

        mRefresh = view.findViewById(R.id.notifications_recycler_swipe);
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.reload(new ReloadDoneCallback() {
                   @Override
                    public void onReloadDone() {
                       mRefresh.setRefreshing(false);
                   }
                });
            }
        });

        return view;
    }

    private final BroadcastReceiver mReloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!Objects.equals(intent.getAction(), mReminderService.getConfigs().getChangeAvailableBroadcastString()))
                return;
            mAdapter.reload(null);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        mPopupService.popup(getContext(),"any");
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(mReloadReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(requireActivity())
                .registerReceiver(mReloadReceiver,
                        new IntentFilter(mReminderService.getConfigs().getChangeAvailableBroadcastString())
                );
    }

    @Override
    public void onStop() {
        super.onStop();
        mPopupService.popup(getContext(),"any");
        LocalBroadcastManager.getInstance(requireActivity())
                .registerReceiver(mReloadReceiver,
                        new IntentFilter(mReminderService.getConfigs().getChangeAvailableBroadcastString())
                );
    }

    private final IOnRecyclerItemLongClick mItemLongClickHandler = new IOnRecyclerItemLongClick() {
        @Override
        public void onItemLongClick(int itemPosition) {
            getCabHost().startCab(mCabCallback);

            // Start CAB visuals in adapter
            mAdapter.setCabMode(mRecycler, true);
        }
    };

    private final ActionMode.Callback mCabCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.notifications_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) { return false; }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int selectionCount;
            switch(item.getItemId()) {
                case R.id.notifications_menu_selectall:
                    mAdapter.selectAll(mRecycler);
                    return true;
//                case R.id.notification_mark_unseen_button:
//                    selectionCount = mAdapter.selectionCount(mRecycler);
//                    if(selectionCount < 1) return true;
//
//                    mAdapter.markUnseenSelected(mRecycler);
//                    mAdapter.reload(null);
//                    mode.finish();
//
//                    if(selectionCount > 1) showSnackbar(mCoordinator, getString(R.string.notifications_deleted));
//                    else showSnackbar(mCoordinator, getString(R.string.notifications_deleted));
//                    return true;
                case R.id.notifications_menu_delete:
                    selectionCount = mAdapter.selectionCount(mRecycler);
                    if(selectionCount < 1) return true;

                    mAdapter.deleteSelected(mRecycler);
                    mAdapter.reload(null);
                    mode.finish();

                    if(selectionCount > 1) showSnackbar(mCoordinator, getString(R.string.notifications_deleted));
                    else showSnackbar(mCoordinator, getString(R.string.notification_deleted));
                    return true;
                default: return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mAdapter.setCabMode(mRecycler, false);
            getCabHost().onCabStopped();
        }
    };
}
