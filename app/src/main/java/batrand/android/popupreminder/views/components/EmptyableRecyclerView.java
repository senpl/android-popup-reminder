package batrand.android.popupreminder.views.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Adapter;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Source: https://stackoverflow.com/a/30415582/6478240
 */

public class EmptyableRecyclerView extends BaseEmptyableRecyclerView {
    private View emptyView;

    private RecyclerView.AdapterDataObserver emptyObserver = new RecyclerView.AdapterDataObserver() {


        @Override
        public void onChanged() {
            RecyclerView.Adapter<?> adapter =  getAdapter();
            if(adapter != null && emptyView != null) {
                if(adapter.getItemCount() == 0) {
                    emptyView.setVisibility(View.VISIBLE);
                    EmptyableRecyclerView.this.setVisibility(View.GONE);
                }
                else {
                    emptyView.setVisibility(View.GONE);
                    EmptyableRecyclerView.this.setVisibility(View.VISIBLE);
                }
            }

        }
    };

    public EmptyableRecyclerView(Context context) {
        super(context);
    }

    public EmptyableRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EmptyableRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);

        if(adapter != null) {
            adapter.registerAdapterDataObserver(emptyObserver);
        }

        emptyObserver.onChanged();
    }

    @Override public void setEmptyView(View emptyView) { this.emptyView = emptyView; }
}
