package batrand.android.popupreminder.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.models.reminder.Reminder;
import batrand.android.popupreminder.models.reminder.ReminderType;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.views.ReminderDetailActivity;
import batrand.android.popupreminder.views.bases.BaseFragment;
import batrand.android.popupreminder.views.callbacks.IOnRecyclerItemLongClick;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

import com.maltaisn.recurpicker.Recurrence;
import com.maltaisn.recurpicker.RecurrenceFormat;

/**
 * Created by batra on 2017-05-23.
 */

public class RemindersAdapter extends RecyclerView.Adapter<RemindersAdapter.ViewHolder> {

    private List<Reminder> mReminders;
    private final Context mContext;
    private BaseFragment mHostFragment;
    private IOnRecyclerItemLongClick mItemLongClickHandler;
    @Inject IReminderService mService;
    @Inject IFormatService mFormatter;
    private Handler mRetrievalHandler = new Handler();

    public RemindersAdapter(BaseFragment hostFragment, @Nullable IOnRecyclerItemLongClick itemLongClick) {
        mContext = hostFragment.getActivity();
        mHostFragment = hostFragment;
        if(itemLongClick != null) mItemLongClickHandler = itemLongClick;
        getInjector().inject(this);
        reload(null);
    }

    public void reload(final ReloadDoneCallback callback) {
        mRetrievalHandler.post(new Runnable() {
            @Override
            public void run() {
                setReminders(mService.getAllReminders());
                notifyDataSetChanged();
                if(callback != null) callback.onReloadDone();
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View reminderView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_reminder, parent, false);
        return new ViewHolder(reminderView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Reminder reminder = getReminders().get(position);
        holder.setReminder(reminder);
    }

    @Override
    public int getItemCount() {
        if(getReminders() == null) return 0;
        return getReminders().size();
    }

    /**
     * @param isEntering true means entering CAB, false means exiting CAB
     */
    public void setCabMode(RecyclerView remindersRecycler, final boolean isEntering) {
        executeOnEachHolder(remindersRecycler, new IExecuteWithHolder() {
            @Override
            public void executeWithHolder(ViewHolder holder, int holderPosition) {
                holder.setSelectionVisibility(isEntering);

                // Reset all checkboxes, whether entering or exiting
                holder.setSelected(false);
            }
        });
    }

    public void selectAll(RecyclerView remindersRecycler) {
        executeOnEachHolder(remindersRecycler, new IExecuteWithHolder() {
            @Override
            public void executeWithHolder(ViewHolder holder, int holderPosition) {
                holder.setSelected(true);
            }
        });
    }

    public void deleteSelected(RecyclerView remindersRecycler) {
        executeOnEachHolder(remindersRecycler, new IExecuteWithHolder() {
            @Override
            public void executeWithHolder(ViewHolder holder, int holderPosition) {
                if(holder.isSelected())
                    mService.unregisterReminder(mContext, getReminders().get(holderPosition).id());
            }
        });
    }

    public void deleteSelected(int remainderPosition) {
        mService.remveSubtask(mContext, getReminders().get(remainderPosition).id());
        mService.unregisterReminder(mContext, getReminders().get(remainderPosition).id());
    }

//    public int selectionCount(RecyclerView remindersRecycler) {
//        int selectionCount = 0;
//        for(int i = 0; i < getReminders().size(); i++) {
//            ViewHolder holder = (ViewHolder) remindersRecycler.findViewHolderForAdapterPosition(i);
//            if(holder != null) {
//                if(holder.isSelected()) selectionCount++;
//            }
//        }
//        return selectionCount;
//    }

    /**
     * Run the code in the execution on all ViewHolders of all items
     */
    private void executeOnEachHolder(RecyclerView recycler, IExecuteWithHolder execution) {
        for(int i = 0; i < getReminders().size(); i++) {
            ViewHolder holder = (ViewHolder) recycler.findViewHolderForAdapterPosition(i);
            if(holder != null) {
                execution.executeWithHolder(holder, i);
            }
        }
    }

    public List<Reminder> getReminders() {
        return mReminders;
    }

    public void setReminders(List<Reminder> mReminders) {
        this.mReminders = mReminders;
    }

    private interface IExecuteWithHolder {
        /**
         * @param holderPosition position of the ViewHolder in the adapter
         */
        void executeWithHolder(ViewHolder holder, int holderPosition);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitleTextView;
        private TextView mDescriptionTextView;
        private TextView mOverviewTextView;
        private Reminder mReminder;
        private final AppCompatCheckBox mSelectionCheckbox;

        ViewHolder(View itemView) {
            super(itemView);

            mTitleTextView = itemView.findViewById(R.id.reminder_card_title);
            mDescriptionTextView = itemView.findViewById(R.id.reminder_card_description);
            mSelectionCheckbox = itemView.findViewById(R.id.reminder_selection_checkbox);
            mOverviewTextView = itemView.findViewById(R.id.reminder_overview);

            if(mItemLongClickHandler != null) {
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if(mItemLongClickHandler == null) return false;
                        mItemLongClickHandler.onItemLongClick(getAdapterPosition());
                        setSelected(true);
                        return true;
                    }
                });
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mSelectionCheckbox.getVisibility() == View.VISIBLE)
                        setSelected(!mSelectionCheckbox.isChecked());
                    // Not in CAB. Open edit mode.
                    else {
                        Intent intent = new Intent(mContext, ReminderDetailActivity.class);
                        intent.putExtra(ReminderDetailActivity.EDIT_MODE_KEY, mReminder.id());
                        mHostFragment.startActivityForResult(intent, ReminderDetailActivity.EDIT_REQUEST);
                    }
                }
            });
        }

        void setReminder(Reminder reminder) {
            mReminder = reminder;

            mTitleTextView.setText(mReminder.getTitle());
            mDescriptionTextView.setText(mReminder.getDescription());

            DateFormat dateFormatShort = new SimpleDateFormat("dd-MM-yyyy", Locale.GERMANY);  // 31-12-2017
            RecurrenceFormat recurrenceFormat = new RecurrenceFormat(mContext, dateFormatShort);
            // Overview text
            if(reminder.getReminderType() == ReminderType.TRIGGER) {
                // Not triggered
                if(!reminder.isTriggered())
                    mOverviewTextView.setText(mContext.getString(R.string.reminder_overview_trigger,
                            mFormatter.formatDateTimeCompact(mContext, reminder.getNextAlarmMillis(mService.getConfigs()))));
                // Triggered
                else
                    mOverviewTextView.setText(mContext.getString(R.string.reminder_overview_triggered,
                            mFormatter.formatDateTimeCompact(mContext, reminder.getLastTriggerTime())));
            }
            if(reminder.getReminderType() == ReminderType.INTERVAL) {
                Recurrence recurrence=new Recurrence(reminder.getRecurrenceData(),0);
                mOverviewTextView.setText(
                        mFormatter.formatDateTimeCompact(mContext,reminder.getNextAlarmMillis(mService.getConfigs()))
                        + " " + recurrenceFormat.format(recurrence));
//                // Interval type string - singular/plural
//                String intervalString = reminder.getIntervalType().toString().toLowerCase();
//                if (reminder.getInterval() > 1) intervalString += "s";
//
//                mOverviewTextView.setText(mContext.getString(R.string.reminder_overview_interval,
//                        Integer.toString(reminder.getInterval()),
//                        intervalString,
//                        mFormatter.formatDateTimeCompact(reminder.getIntervalStart())));
            }
        }

        void setSelectionVisibility(boolean isCheckboxVisible) {
            if(isCheckboxVisible) mSelectionCheckbox.setVisibility(View.VISIBLE);
            else mSelectionCheckbox.setVisibility(View.GONE);
        }

        void setSelected(boolean isSelected) {
            mSelectionCheckbox.setChecked(isSelected);
        }

        boolean isSelected() { return mSelectionCheckbox.isChecked(); }
    }
}
