package batrand.android.popupreminder.views.callbacks;

import android.annotation.SuppressLint;

import androidx.appcompat.view.ActionMode;

/**
 * Activity that hosts the contextual action bar
 */

public interface IMainCabActivity {

    void startCab(ActionMode.Callback callback);

//    @SuppressLint("RestrictedApi")
//    void startCab(ActionMode.Callback callback);

    void onCabStopped();
}
