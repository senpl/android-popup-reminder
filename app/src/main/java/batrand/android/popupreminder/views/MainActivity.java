package batrand.android.popupreminder.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import java.util.Objects;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.FragmentManager;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.services.settings.ISettingsService;
import batrand.android.popupreminder.views.bases.BaseActivity;
import batrand.android.popupreminder.views.bases.BaseFragment;
import batrand.android.popupreminder.views.callbacks.IMainCabActivity;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

public class MainActivity extends BaseActivity {

    @Inject IPopupService mPopupService;
    @Inject IReminderService mReminderService;
    @Inject ISettingsService mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getInjector().inject(this);

        // Setup custom action bar
        Objects.requireNonNull(getSupportActionBar()).setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.view_titlebar);

        // Set up fragments
        BottomBarButtonHandler remindersHandler = new BottomBarButtonHandler(
                findViewById(R.id.main_bottomButton_reminders),
                RemindersFragment.newInstance(mCabController)
        );
        BottomBarButtonHandler notificationsHandler = new BottomBarButtonHandler(
                findViewById(R.id.main_bottomButton_notifications),
                NotificationsFragment.newInstance(mCabController)
        );
        BottomBarButtonHandler notificationsHandler2 = new BottomBarButtonHandler(
                findViewById(R.id.main_bottomButton_notifications2),
                SeenFragment.newInstance(mCabController)
        );


        // Handle show internet permission rationale
        if(!mSettings.getHasSeenInternetPermissionRationale(this)) {
            startActivity(new Intent(this, InternetRationaleActivity.class));
        }

        // Open first screen
        //TODO Remove from final version
        BottomBarHandler mBottomBar = new BottomBarHandler(
                new BottomBarButtonHandler[]{remindersHandler,
                        notificationsHandler,
                        notificationsHandler2},
                findViewById(R.id.main_bottomButton_menu),
                findViewById(R.id.main_bottomButton_menu_anchor),
                this, R.menu.main_bottom_menu,
                getSupportFragmentManager(), R.id.main_container
        );

//        mBottomBar.select(1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!mPopupService.hasPermission(this)) {
            mPopupService.processPermission(this);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        mPopupService.popup(this,"any");
        finish();
    }

    private final IMainCabActivity mCabController = new IMainCabActivity() {
        @SuppressLint("RestrictedApi")
        @Override
        public void startCab(ActionMode.Callback callback) {
            Objects.requireNonNull(getSupportActionBar()).startActionMode(callback);
            int mStatusBarColorActionMode = R.color.colorAccent;
            getWindow().setStatusBarColor(getResources().getColor(mStatusBarColorActionMode));
        }

        @Override
        public void onCabStopped() {
            //region CAB
            int mStatusBarColor = R.color.colorPrimaryDark;
            getWindow().setStatusBarColor(getResources().getColor(mStatusBarColor));
        }
    };
    //endregion

    /**
     * Abstraction layer for bottom bar buttons
     */
    private static class BottomBarButtonHandler {
        private final View mButton;
        View getButton() { return mButton; }

        private final BaseFragment mFragment;
        BaseFragment getFragment() { return mFragment; }

        BottomBarButtonHandler(View button, BaseFragment fragment) {
            mButton = button;
            mFragment = fragment;
        }

        void select(boolean isSelected) {
            mButton.setActivated(isSelected);
        }
    }

    private class BottomBarHandler {
        private final BottomBarButtonHandler[] mButtons;
        private final FragmentManager mManager;
        private final int mContainerId;
        private int mCurrentIndex = -1;

        BottomBarHandler(BottomBarButtonHandler[] buttons,
                         View menuButton, View menuAnchor, Context contextWithStyle, int menuRId,
                         FragmentManager manager, int containerId) {
            mButtons = buttons;
            mManager = manager;
            mContainerId = containerId;

            setupClickListeners();
            setupMenu(menuButton, menuAnchor, contextWithStyle, menuRId);
        }

        void select(int buttonIndex) {
            if(mCurrentIndex == buttonIndex) return;

            for(int i = 0; i < mButtons.length; i++) {
                if(i == buttonIndex) mButtons[i].select(true);
                else mButtons[i].select(false);
            }

            mManager.beginTransaction()
                    .replace(mContainerId, mButtons[buttonIndex].getFragment())
                    .commit();

            mCurrentIndex = buttonIndex;
        }

        private void setupClickListeners() {
            for(int i = 0; i < mButtons.length; i++) {
                final int index = i;
                mButtons[i].getButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        select(index);
                    }
                });
            }
        }

        private View mMenuButton;
        private PopupMenu mMenu;

        private void setupMenu(View button, View anchor, final Context context, int menuRId) {
            mMenuButton = button;
            mMenuButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMenu.show();
                    mMenuButton.setActivated(true);
                }
            });

            mMenu = new PopupMenu(context, anchor);
            mMenu.inflate(menuRId);
            mMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch(item.getItemId()) {
                        case R.id.bottommenuitem_settings:
                            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                            return true;
                        case R.id.bottommenuitem_about:
                            startActivity(new Intent(MainActivity.this, AboutActivity.class));
                            return true;
                        default: return false;
                    }
                }
            });
            mMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                @Override
                public void onDismiss(PopupMenu menu) {
                    mMenuButton.setActivated(false);
                }
            });
        }
    }

}
