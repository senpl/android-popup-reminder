package batrand.android.popupreminder.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import batrand.android.popupreminder.R;

public class AboutSoundsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_sounds);

        findViewById(R.id.about_sound_linkbutton_pingding).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.about_sound_pingding_link)));
                startActivity(intent);
            }
        });

        findViewById(R.id.about_sound_linkbutton_messageding).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.about_sound_messageding_link)));
                startActivity(intent);
            }
        });
    }
}
