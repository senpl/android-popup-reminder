package batrand.android.popupreminder.models.reminder;

/**
 * Created by batra on 2017-06-02.
 */

public class ReminderIntervalType {
    //region Private constants
    private static final int VALUE_MINUTE = 0;
    private static final int VALUE_HOUR = 1;
    private static final int VALUE_DAY = 2;
    private static final int VALUE_WEEK = 3;
    private static final int VALUE_MONTH = 4;
    private static final int VALUE_YEAR = 5;

    private static final String STRING_MINUTE = "MINUTE";
    private static final String STRING_HOUR = "HOUR";
    private static final String STRING_DAY = "DAY";
    private static final String STRING_WEEK = "WEEK";
    private static final String STRING_MONTH = "MONTH";
    private static final String STRING_YEAR = "YEAR";

    private static final String EXCEPTION_INVALID_VALUE = "The value does not match any supported ReminderIntervalType.";
    //endregion

    public static final ReminderIntervalType MINUTE = new ReminderIntervalType(VALUE_MINUTE, STRING_MINUTE);
    public static final ReminderIntervalType HOUR = new ReminderIntervalType(VALUE_HOUR, STRING_HOUR);
    public static final ReminderIntervalType DAY = new ReminderIntervalType(VALUE_DAY, STRING_DAY);
    public static final ReminderIntervalType WEEK = new ReminderIntervalType(VALUE_WEEK, STRING_WEEK);
    public static final ReminderIntervalType MONTH = new ReminderIntervalType(VALUE_MONTH, STRING_MONTH);
    public static final ReminderIntervalType YEAR = new ReminderIntervalType(VALUE_YEAR, STRING_YEAR);

    private int mValue;
    private String mString;
    private ReminderIntervalType(int value, String string) {
        mValue = value;
        mString = string;
    }

    public int toInt() { return mValue; }
    public String toString() { return mString; }

    public static ReminderIntervalType from(int value) {
        switch(value) {
            case VALUE_MINUTE: return MINUTE;
            case VALUE_HOUR: return HOUR;
            case VALUE_DAY: return DAY;
            case VALUE_WEEK: return WEEK;
            case VALUE_MONTH: return MONTH;
            case VALUE_YEAR: return YEAR;
            default: throw new IllegalArgumentException(EXCEPTION_INVALID_VALUE);
        }
    }

    public static ReminderIntervalType from(String string) {
        switch(string) {
            case STRING_MINUTE: return MINUTE;
            case STRING_HOUR: return HOUR;
            case STRING_DAY: return DAY;
            case STRING_WEEK: return WEEK;
            case STRING_MONTH: return MONTH;
            case STRING_YEAR: return YEAR;
            default: throw new IllegalArgumentException(EXCEPTION_INVALID_VALUE);
        }
    }
}
