package batrand.android.popupreminder.models;

import io.realm.RealmObject;

/**
 * Created by batra on 2017-06-07.
 */

public class LogEntry extends RealmObject {
    public LogEntry() { timestamp = System.currentTimeMillis(); }
    public long timestamp;
    public String message;
}
