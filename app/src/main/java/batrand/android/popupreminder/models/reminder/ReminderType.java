package batrand.android.popupreminder.models.reminder;

/**
 * Created by batra on 2017-06-02.
 */

public class ReminderType {
    //region Private constants
    private static final int VALUE_TRIGGER = 1;
    private static final int VALUE_INTERVAL = 2;

    private static final String STRING_TRIGGER = "TRIGGER";
    private static final String STRING_INTERVAL = "INTERVAL";

    private static final String EXCEPTION_INVALID_VALUE = "The value does not match any supported ReminderType.";
    //endregion

    public static final ReminderType TRIGGER = new ReminderType(VALUE_TRIGGER, STRING_TRIGGER);
    public static final ReminderType INTERVAL = new ReminderType(VALUE_INTERVAL, STRING_INTERVAL);

    private int mValue;
    private String mString;
    private ReminderType(int value, String string) {
        mValue = value;
        mString = string;
    }

    public int toInt() { return mValue; }
    public String toString() { return mString; }

    public static ReminderType from(int value) {
        switch(value) {
            case VALUE_TRIGGER: return TRIGGER;
            case VALUE_INTERVAL: return INTERVAL;
            default: throw new IllegalArgumentException(EXCEPTION_INVALID_VALUE);
        }
    }

    public static ReminderType from(String string) {
        switch(string) {
            case STRING_TRIGGER: return TRIGGER;
            case STRING_INTERVAL: return INTERVAL;
            default: throw new IllegalArgumentException(EXCEPTION_INVALID_VALUE);
        }
    }
}
