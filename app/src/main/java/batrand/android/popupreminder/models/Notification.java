package batrand.android.popupreminder.models;

import java.util.List;
import java.util.UUID;

import batrand.android.popupreminder.models.reminder.Reminder;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by batra on 2017-05-24.
 */

public class Notification extends RealmObject {

    // Required for Realm
    public Notification() {}
    public Notification(Reminder reminder) {
        id = UUID.randomUUID().toString();
        order=System.currentTimeMillis();
        reminderId = reminder.id();
        reminderTitle = reminder.getTitle();
        reminderDescription = reminder.getDescription();
        taskContext = reminder.getTaskContext();
        timestamp = System.currentTimeMillis();
        hideHour = reminder.getHideHour();
        hideMinute = reminder.getHideMinute();
        isSeen = false;
//        subtasks=reminder.getSubtasks();
    }

//    private List<Subtask> subtasks;

    @PrimaryKey private String id;
    public String id() { return id; }


    private String reminderId;
    public String reminderId() { return reminderId; }

    private String reminderTitle;
    public String reminderTitle() { return reminderTitle; }

    private String reminderDescription;
    public String reminderDescription() { return reminderDescription; }

    private String taskContext;

    public String taskContext() {
        return taskContext;
    }

    private long order;

    public long order() {
        return order;
    }

    private long timestamp;

    public long timestamp() {
        return timestamp;
    }

    private int hideHour;

    public int getHideHour(){
        return hideHour;
    }

    private int hideMinute;
    public int getHideMinute(){
        return hideMinute;
    }

    private boolean isSeen;
    public boolean isSeen() { return isSeen; }
    public void markSeen() { isSeen = true; }

    public long getOrder() {
        return order;
    }

    public void setOrder(long orderToSet) {
        order = orderToSet;
    }

//    public List<Subtask> getSubtasks() {
//        return subtasks;
//    }
//
//    public void setSubtasks(List<Subtask> subtasks) {
//        this.subtasks = subtasks;
//    }
}
