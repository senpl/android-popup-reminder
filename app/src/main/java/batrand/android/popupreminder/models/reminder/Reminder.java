package batrand.android.popupreminder.models.reminder;

import com.maltaisn.recurpicker.Recurrence;

import java.util.List;
import java.util.UUID;

import batrand.android.popupreminder.services.reminder.IReminderService;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by senpl on work of batra . Hide task by senpl
 */

public class Reminder extends RealmObject {

    // Required for Realm
    public Reminder() {
        id = UUID.randomUUID().toString();
        timestamp = System.currentTimeMillis();
    }

    @PrimaryKey private String id;
    public String id() { return id; }

    private long timestamp;
    public long timestamp() { return timestamp; }

    private long order;
    public long getOrder() {
        return order;
    }
    public void setOrder(long order) {
        this.order = order;
    }

    private String title;
    public void setTitle(String title) { this.title = title; }
    public String getTitle() { return title; }

    private String description;
    public void setDescription(String description) { this.description = description; }
    public String getDescription() { return description; }

    private String context;

    public void setTaskContext(String context) {
        this.context = context;
    }

    public String getTaskContext() {
        return context;
    }

    private int subTasksMainId;

    public boolean isValidReminder(IReminderService.IConfigs reminderConfigs) {
        boolean isValid = true;
//        if(reminderType == -1) isValid = false;
        if(getReminderType() == ReminderType.TRIGGER)
            if(triggerTime == -1 || triggerTime < System.currentTimeMillis()) isValid = false;

//        else if(getReminderType() == ReminderType.INTERVAL)
//            if(interval == -1 && !reminderConfigs.isValidInterval(getIntervalMillis(interval, getIntervalType())))
//                isValid = false;
        return isValid;
    }

    private int reminderType = -1;
    public void setReminderType(ReminderType type) { reminderType = type.toInt(); }
    public ReminderType getReminderType() { return ReminderType.from(reminderType); }

    private byte[] recurrenceData;
    public void setRecurrenceData(byte[] recurrenceDataArray) { recurrenceData = recurrenceDataArray; }
    public byte[] getRecurrenceData() { return recurrenceData; }

    private long triggerTime = -1;
    public long getTriggerTime() { return triggerTime; }
    public void setAsTrigger(long triggerTime) {
        setReminderType(ReminderType.TRIGGER);
        lastTriggerTime = -1; // reset on edit
        this.triggerTime = triggerTime;
    }


    private long lastTriggerTime = -1;
    public void trigger() { lastTriggerTime = System.currentTimeMillis(); }
    public boolean isTriggered() { return lastTriggerTime != -1; }
    public long getLastTriggerTime() { return lastTriggerTime; }

    /**
     * From current reminder gets next trigger.
     * @param configs unused
     * @return long with Milliseconds to trigger next reminder
     */
    public long getNextAlarmMillis(IReminderService.IConfigs configs) {
        //Trigger
        if(getReminderType() == ReminderType.TRIGGER) return triggerTime;

        // Interval
        else {
            // Interval not triggered, next time is startTime.
//            if(!isTriggered()) return intervalStart;
//
//            // Interval already triggered. Next trigger is a number of interval times
//            // since intervalStart that is greater than current system time.
//            else {
                Recurrence recurrence=new Recurrence(getRecurrenceData(),0);
                if(recurrence.getStartDate()>System.currentTimeMillis())
                    return recurrence.getStartDate();
                else {
                    long bufferForNextTask=1400;
                    List<Long> foundRecurrenceList = recurrence.findRecurrences(System.currentTimeMillis()+bufferForNextTask, 1);
                    long foundRecurrence = 0;
                    if (!foundRecurrenceList.isEmpty())
                        foundRecurrence = foundRecurrenceList.get(0);
                    return foundRecurrence;
                }
        }
    }

    /**
     * @return true if this is a trigger that is not triggered and is late
     */
    public boolean isLateTrigger() {
        return getReminderType() == ReminderType.TRIGGER
                && !isTriggered()
                && System.currentTimeMillis() > triggerTime;
    }

    private int hideHour;
    public void setHideHour(int hideHourToSet) {
        hideHour = hideHourToSet;
    }

    public int getHideHour() {
        return hideHour;
    }

    private int hideMinute;

    public int getHideMinute() {
        return hideMinute;
    }

    public void setHideMinute(int hideMinuteToSet) {
        hideMinute=hideMinuteToSet;
    }

    private int hideBeforeHour;
    public int getHideBeforeHour() {
        return hideBeforeHour;
    }
    public void setHideBeforeHour(int hideBeforeHourToSet) {
        hideBeforeHour=hideBeforeHourToSet;
    }

    private int hideBeforeMinute;
    public int getHideBeforeMinute() {
        return hideBeforeMinute;
    }
    public void setHideBeforeMinute(int hideBeforeMinuteToSet) {
        hideBeforeMinute=hideBeforeMinuteToSet;
    }

    public int getSubTasksMainId() {
        return subTasksMainId;
    }

    public void setSubTasksMainId(int subTasksMainId) {
        this.subTasksMainId = subTasksMainId;
    }
}
