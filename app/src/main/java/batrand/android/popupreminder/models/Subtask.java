package batrand.android.popupreminder.models;


import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by senpl.
 */

public class Subtask extends RealmObject {

    public Subtask(){
        id = UUID.randomUUID().toString();
        order=System.currentTimeMillis();
        this.remainderId =id;
        title = "";
        isDone=false;
    }
    // Required for Realm
    public Subtask(String remainderId) {
        id = UUID.randomUUID().toString();
        order=System.currentTimeMillis();
        this.remainderId =remainderId;
        title = "";
        isDone=false;
    }
    public Subtask(Subtask subtask) {
        id = UUID.randomUUID().toString();
        this.remainderId=subtask.remainderId;
        order=subtask.getOrder();
        title = subtask.getTitle();
        isDone = false;
    }

    @PrimaryKey private String id;
    public String id() { return id; }

    private String title;
    public String getTitle() { return title; }

    private long order;

    public long order() {
        return order;
    }

    private int timestamp;

    private boolean isDone;
    public boolean isDone() { return isDone; }
    public void markDone() { isDone = true; }

    public long getOrder() {
        return order;
    }

    public void setOrder(long orderToSet) {
        order = orderToSet;
    }

    private String remainderId;
    public String getRemainderId() {
        return remainderId;
    }

    public void setTitle(String titleToSet) {
        title=titleToSet;
    }
}
