package batrand.android.popupreminder.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import javax.inject.Inject;

import batrand.android.popupreminder.services.log.ILog;
import batrand.android.popupreminder.services.reminder.IReminderService;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

/**
 * Listen for package changes e.g. updates.
 */
public class PackageReplacedReceiver extends BroadcastReceiver {
    @Inject IReminderService mReminderService;
    @Inject ILog mLog;

    public PackageReplacedReceiver() {
        getInjector().inject(this);
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.MY_PACKAGE_REPLACED")) {
            mLog.log("Package replaced.");
            mReminderService.resetAlarms(context);
        }
    }
}
