package batrand.android.popupreminder.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import javax.inject.Inject;

import batrand.android.popupreminder.services.alarm.IAlarmService;
import batrand.android.popupreminder.services.reminder.IReminderService;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

public class AlarmTriggerReceiver extends BroadcastReceiver {

    @Inject IReminderService mReminderService;
    @Inject IAlarmService mAlarmService;

    @Override
    public void onReceive(Context context, Intent intent) {
        getInjector().inject(this);
        if(intent.getAction().equals(mAlarmService.getConfigs().getTriggerAction(context))
           && intent.getExtras() != null) {
            String triggeredId = intent.getExtras().getString(mAlarmService.getConfigs().getIdKey());
            mReminderService.onReminderTriggered(context, triggeredId);
//            Log.d("BABA", "AlarmTriggerReceiver finished");
        }
    }
}
