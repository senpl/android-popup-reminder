package batrand.android.popupreminder.android;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.preference.PreferenceManager;
import javax.inject.Inject;
import batrand.android.popupreminder.BuildConfig;
import batrand.android.popupreminder.R;
import batrand.android.popupreminder.services.settings.ISettingsService;
import io.realm.Realm;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

/**
 * Created by batra on 2017-05-03.
 */

public class PopupReminderApplication extends Application {
    @Inject ISettingsService mSettings;
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        getInjector().inject(this);

//        if(shouldEnableCrashlytics()) {
////            Fabric.with(this, new Crashlytics());
//        }

        registerDefaultNotifChannel();
    }

    private boolean shouldEnableCrashlytics() {
        // When user allows to
        return mSettings.getShouldReportCrashes(this)
                // and when it's not a debug build
                && !BuildConfig.DEBUG;
    }

    private void registerDefaultNotifChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        // Once called, will register with the system, and subsequent
        // calls will not make any difference.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.notification_channel_name);
            String description = getString(R.string.notification_channel_description);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(getString(R.string.notification_channel_id), name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
