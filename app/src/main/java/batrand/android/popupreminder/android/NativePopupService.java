package batrand.android.popupreminder.android;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.Build;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.models.Notification;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.popup.IPopupSoundService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.services.settings.ISettingsService;
import batrand.android.popupreminder.views.adapters.NotificationsAdapter;
import batrand.android.popupreminder.views.components.BaseEmptyableRecyclerView;
import io.realm.Realm;

import static android.content.res.Resources.getSystem;
import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

// Reference: http://androidsrc.net/facebook-chat-like-floating-chat-heads/
public class NativePopupService extends Service {

    @Inject IPopupService mPopupService;
    @Inject IReminderService mReminderService;
    @Inject IPopupSoundService mPopupSound;
    @Inject IFormatService mFormatter;
    @Inject ISettingsService mSettings;
    private IPopupService.IConfigs popupConfigs() { return mPopupService.getConfigs(); }
    private IReminderService.IConfigs reminderConfigs() { return mReminderService.getConfigs(); }

    //region Construction, lifecycle, injection
    public NativePopupService() {}

    @Override
    public IBinder onBind(Intent intent) { return null; }

    @Override
    public void onCreate() {
        super.onCreate();
        getInjector().inject(this);
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mLayoutInflater = LayoutInflater.from(this);
        mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mChangeReceiver,
                new IntentFilter(reminderConfigs().getChangeAvailableBroadcastString()));
        setAsForeground();
        setupViews();
        setFloatingViewVisibleAnimated(true, null);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        processStartIntent(intent);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mChangeReceiver);
        unsetAsForeground();
        removeViews();
    }

    private final BroadcastReceiver mChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!intent.getAction().equals(reminderConfigs().getChangeAvailableBroadcastString()))
                return;
            reloadExpandedView();
        }
    };

    private void processStartIntent(Intent intent) {
        if(intent == null) return;
        if(intent.getExtras() == null) return;
        String command = intent.getExtras().getString(popupConfigs().getCommandKey());
        if(command == null) return;

        // Cannot use switch - not constant expressions
        // Dismiss
        if(command.equals(popupConfigs().getDismissCommand())) {
            stopSelf();
        }
        // Popup
        else if(command.startsWith(popupConfigs().getPopupCommand())) {
            // TODO: retrieve notification/reminder, show title
            loadBadgeCount();
            if(mSettings.shouldPlayPopupSound(this)) mPopupSound.playSound(this);
        }
    }

    private void animatedStopSelf() {
        // UndoExpand if expanded view is expanded,
        // animate floater view away,
        // then on animation end, animate dismiss view away,
        // then on animation end, stop service.
        setFloatingViewVisibleAnimated(false, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if(mIsExpanded) setExpandedViewExpanded(false);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                setDismissViewVisibleAnimated(false, new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        stopSelf();
                    }
                });
            }
        });
    }

    //endregion

    //region Foregrounding
    private static final int FOREGROUND_ID = 15487;

    private void setAsForeground() {
        Intent appIntent = new Intent(this, popupConfigs().getPendingOpenActivityClass());
        PendingIntent appPending = PendingIntent.getActivity(this, 0, appIntent, 0);

        Intent dismissIntent = popupConfigs().getDismissIntent(this);
        PendingIntent dismissPending = PendingIntent.getService(this, 0, dismissIntent, 0);

        NotificationCompat.Builder notif;

        // Set up notification, handling Android O's requirement for a channel ID
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            notif = new NotificationCompat.Builder(this)
                    .setContentTitle(getText(R.string.floater_title))
                    .setContentText(getText(R.string.floater_description))
                    .setContentIntent(appPending);
        }
        else {
            notif = new NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
                    .setContentTitle(getText(R.string.floater_title))
                    .setContentText(getText(R.string.floater_description))
                    .setContentIntent(appPending)
                    .setPriority(NotificationCompat.PRIORITY_LOW);
        }

        // Use raster icons for pre-Lollipop
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            notif.setSmallIcon(R.drawable.app_icon_nobg_rast_small)
                    .addAction(R.drawable.close_black_rast_small,getText(R.string.close),dismissPending);
        }
        else {
            notif.setSmallIcon(R.drawable.app_icon_nobg)
                    .addAction(R.drawable.close_black,getText(R.string.close),dismissPending);
        }

        int unseenNotifs = mReminderService.getUnseenNotificationCount();
        if(unseenNotifs > 0) {
            Resources resources = getResources(),
                    systemResources = getSystem();
            // Turn on notification lights using default color and rate
            notif.setLights(
                    ContextCompat.getColor(this, systemResources
                            .getIdentifier("config_defaultNotificationColor", "color", "android")),
                    resources.getInteger(systemResources
                            .getIdentifier("config_defaultNotificationLedOn", "integer", "android")),
                    resources.getInteger(systemResources
                            .getIdentifier("config_defaultNotificationLedOff", "integer", "android")));
            // Vibrate
            if(mSettings.getShouldVibrate(this)) vibrate();
        }

        startForeground(FOREGROUND_ID, notif.build());
    }

    private void unsetAsForeground() { stopForeground(true); }
    //endregion

    //region Window Manager & View management
    private WindowManager mWindowManager;
    private LayoutInflater mLayoutInflater;

    private void addView(View view, WindowManager.LayoutParams params) {
        mWindowManager.addView(view, params);
    }

    private void removeView(View view) {
        mWindowManager.removeView(view);
    }

    private void removeViews() {
        removeView(mFloatingView);
        removeView(mDismissView);
        removeView(mExpandedView);
    }

    private void setupViews() {
        setupDismissView();
        setupExpandedView();
        setupFloatingView();
    }

    //region Floating View & Badge
    private RelativeLayout mFloatingView;
    private WindowManager.LayoutParams mFloatingParams;
    private void setupFloatingView() {
        mFloatingView = (RelativeLayout) mLayoutInflater.inflate(popupConfigs().getLayoutRIdForFloatingView(),null);
        mBadgeTextView = mFloatingView.findViewById(popupConfigs().getRIdForBadgeTextView());
        mFloatingViewClose = mFloatingView.findViewById(popupConfigs().getRIdForFloatingViewClosingBackground());
        mFloatingParams = buildParams(Gravity.TOP | Gravity.START, getScreenWidth(), popupConfigs().getStartingFloaterTopOffset());
        addView(mFloatingView, mFloatingParams);
        mFloatingView.setOnTouchListener(new FloatingViewTouchListener());
    }

    private TextView mBadgeTextView;
    private void loadBadgeCount() { setBadgeCount(mReminderService.getNotHiddenNotificationCount()); }
    private void markBadgeRead() { setBadgeCount(0); }
    private void setBadgeCount(int count) {
        mBadgeTextView.setText(String.format(Locale.getDefault(), "%d", count));
        if(count == 0) mBadgeTextView.setVisibility(View.INVISIBLE);
        else mBadgeTextView.setVisibility(View.VISIBLE);
    }
    //endregion

    //region Dismiss View
    private RelativeLayout mDismissView;
    private WindowManager.LayoutParams mDismissParams;
    private void setupDismissView() {
        mDismissView = (RelativeLayout) mLayoutInflater.inflate(popupConfigs().getLayoutRIdForDismissView(), null);
        mDismissParams = buildParams(Gravity.CENTER | Gravity.BOTTOM, 0, 0);
        mDismissView.setAlpha(0);
        mDismissView.setVisibility(View.GONE);
        addView(mDismissView, mDismissParams);
    }
    //endregion


    //region Expanded View
    private RelativeLayout mExpandedView;
    private WindowManager.LayoutParams mExpandedParams;
    private BaseEmptyableRecyclerView mExpandedRecycler;
    private NotificationsAdapter mExpandedAdapter;
    private void setupExpandedView() {
        mExpandedView = (RelativeLayout) mLayoutInflater.inflate(popupConfigs().getLayoutRIdForExpandedView(), null);
        mExpandedParams = buildParams(Gravity.TOP, 0, popupConfigs().getExpandedViewTopOffset());
        mExpandedParams.width = getScreenWidth();
        mExpandedParams.height = getScreenHeight() - popupConfigs().getExpandedViewTopOffset();
        addView(mExpandedView, mExpandedParams);
        mExpandedView.setVisibility(View.GONE);

        mExpandedRecycler = mExpandedView.findViewById(popupConfigs().getRIdForExpandedRecyclerEmptyable());
        mExpandedAdapter = popupConfigs().getExpandedViewAdapter(this);
        mExpandedRecycler.setAdapter(mExpandedAdapter);
        mExpandedRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mExpandedRecycler.setEmptyView(mExpandedView.findViewById(popupConfigs().getRIdForExpandedEmptyView()));

        mExpandedView.findViewById(R.id.quick_add_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent appIntent = new Intent(NativePopupService.this,
                        popupConfigs().getAddActivityClass()
                );
                appIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appIntent);
                animatedStopSelf();
            }
        });

        mExpandedView.findViewById(R.id.expanded_view_open_app_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent appIntent = new Intent(NativePopupService.this, popupConfigs().getPendingOpenActivityClass());
                appIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appIntent);
                animatedStopSelf();
            }
        });
        mExpandedView.findViewById(R.id.close_expanded_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setExpandedViewExpanded(false);
            }
        });
//        mExpandedView.setOnKeyListener(new View.OnKeyListener(){
//            @Override
//            public boolean onKey(View view, int i, KeyEvent keyEvent) {
//                if (keyEvent.getAction() == KeyEvent.KEYCODE_BACK) {
//                setExpandedViewExpanded(false);
////                Log.e("Key Clicked","sen");
////                    Toast.makeText(getBaseContext(),"TEST",Toast.LENGTH_LONG);
//                    return true;
//                }
//                return false;
//            }
//        });

        ItemTouchHelper touchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0){
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder dragged, @NonNull RecyclerView.ViewHolder target) {

                int fromPosition = dragged.getAdapterPosition();
                int toPosition = target.getAdapterPosition();
                List<Notification> listOfNotifications=mExpandedAdapter.getNotifications();
                if (fromPosition < toPosition) {
                    for (int i = fromPosition; i < toPosition; i++) {
                        Collections.swap(listOfNotifications, i, i + 1);

                        long order1 = listOfNotifications.get(i).getOrder();
                        long order2 = listOfNotifications.get(i + 1).getOrder();
                        listOfNotifications.get(i).setOrder(order2);
                        listOfNotifications.get(i + 1).setOrder(order1);
                    }
                }
                else {
                    for (int i = fromPosition; i > toPosition; i--) {
                        Collections.swap(listOfNotifications, i, i - 1);

                        long order1 = listOfNotifications.get(i).getOrder();
                        long order2 = listOfNotifications.get(i - 1).getOrder();
                        listOfNotifications.get(i).setOrder(order2);
                        listOfNotifications.get(i - 1).setOrder(order1);
                    }
                }
                mExpandedAdapter.notifyItemMoved(fromPosition,toPosition);
                final List<Notification> toPersistNotificationsList=listOfNotifications;
                // Persist notifications
                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm tRealm) {
                        tRealm.copyToRealmOrUpdate(toPersistNotificationsList);
                    }
                });
                realm.close();

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            }
        });
        touchHelper.attachToRecyclerView(mExpandedRecycler);
    }


    private void reloadExpandedView() {
        (mExpandedAdapter).reload(null);
    }

    private void reloadNextActionExpandedView() {
        (mExpandedAdapter).reloadNextAction(null);
    }

    private void setExpandedViewExpanded(boolean isExpanded) {
        if(isExpanded) {
            moveFloaterOnExpanded();
            expandExpandedViewAnimated(true, null);
            setFloatingViewClosingBackgroundVisibleAnimated(true);
            if(mSettings.shouldAutoMarkSeen(this)) mReminderService.markAllNotificationsSeen(this);
        }
        else {
            expandExpandedViewAnimated(false, null);
            setFloatingViewClosingBackgroundVisibleAnimated(false);

            // Stop lights
            stopForeground(true);
            setAsForeground();
        }
    }


    private ImageView mFloatingViewClose;

    /**
     * Animate in/out the closing background of the floating view
     */
    private void setFloatingViewClosingBackgroundVisibleAnimated(boolean isVisible) {
        float invisibleAlpha = 0, visibleAlpha = 1;
        float currentAlpha, targetAlpha;
        if(isVisible) { currentAlpha = invisibleAlpha; targetAlpha = visibleAlpha; }
        else { currentAlpha = visibleAlpha; targetAlpha = invisibleAlpha; }

        ValueAnimator va = ValueAnimator.ofFloat(currentAlpha, targetAlpha);
        va.setDuration(popupConfigs().getExpansionAnimationLength());
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mFloatingViewClose.setAlpha((float) animation.getAnimatedValue());
            }
        });
        va.start();
    }
    //endregion

    private WindowManager.LayoutParams buildParams(int gravity, int startX, int startY) {
        WindowManager.LayoutParams params;
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSPARENT);
        }
        else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSPARENT);
        }
        params.gravity = gravity;
        params.x = startX;
        params.y = startY;
        return params;
    }

    private DisplayMetrics getScreenMetrics() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        mWindowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    private int getScreenWidth() { return getScreenMetrics().widthPixels; }
    private int getScreenHeight() { return getScreenMetrics().heightPixels; }

    private Vibrator mVibrator;
    private void vibrate() {
        mVibrator.vibrate(popupConfigs().getVibrationDuration());
    }

    //region Floating View Touch Listener
    private class FloatingViewTouchListener implements View.OnTouchListener {
        private int initialX;
        private int initialY;
        private float initialTouchX;
        private float initialTouchY;

        /**
         * Number of milliseconds below which a motion is considered a single tap
         */
        private final long mSingleTapThreshold = 180;
        private long mLastDown = 0;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    setDismissViewVisibleAnimated(true, null);

                    initialX = mFloatingParams.x;
                    initialY = mFloatingParams.y;
                    initialTouchX = event.getRawX();
                    initialTouchY = event.getRawY();

                    mLastDown = System.currentTimeMillis();

                    return true;
                case MotionEvent.ACTION_UP:
                    if(floatingOverlapsDismiss()) {
                        animatedStopSelf();
                        return true;
                    }

                    setDismissViewVisibleAnimated(false, null);

                    if(isSingleTap()) {
                        markBadgeRead();

                        // Ignore taps when expansion in progress
                        if(mIsExpanding) return true;

                        setExpandedViewExpanded(!mIsExpanded);
                        return true;
                    }

                    // Nothing else: stick floater to screen edge
                    if(isOnLeftHalfOfScreen(mFloatingParams.x)) {
                        stickFloaterToLeftEdge(true);
                    }
                    else {
                        stickFloaterToLeftEdge(false);
                    }

                    return true;
                case MotionEvent.ACTION_MOVE:
                    // Enable when developing floater touch behaviors
                    // setBadgeCount(1);

                    // Closes expanded view on floater moving
                    if(mIsExpanded) setExpandedViewExpanded(false);

                    mFloatingParams.x = initialX + (int) (event.getRawX() - initialTouchX);
                    mFloatingParams.y = initialY + (int) (event.getRawY() - initialTouchY);
                    mWindowManager.updateViewLayout(mFloatingView, mFloatingParams);

                    // If overlap state changes
                    if(mLastIsFloatingOverlapDismiss != floatingOverlapsDismiss()) {
                        // And is changing from not overlapping to overlapping (entering)
                        if(!mLastIsFloatingOverlapDismiss && floatingOverlapsDismiss()) {
                            vibrate();
                        }
                    }
                    mLastIsFloatingOverlapDismiss = floatingOverlapsDismiss();

                    if(isOnLeftHalfOfScreen(mFloatingParams.x)) alignBadgeLeft(false);
                    else alignBadgeLeft(true);

                    return true;
            }

            return false;
        }

        private boolean mLastIsFloatingOverlapDismiss = false;

        private boolean floatingOverlapsDismiss() {
            int[] headPos = new int[2];
            int[] dismissPos = new int[2];

            mFloatingView.getLocationOnScreen(headPos);
            mDismissView.getLocationOnScreen(dismissPos);

            // Rect constructor parameters: left, top, right, bottom
            Rect headRect = new Rect(headPos[0],
                    headPos[1],
                    headPos[0]+mFloatingView.getMeasuredWidth(),
                    headPos[1]+mFloatingView.getMeasuredHeight());
            Rect dismissRect = new Rect(dismissPos[0],
                    dismissPos[1],
                    dismissPos[0]+mDismissView.getMeasuredWidth(),
                    dismissPos[1]+mDismissView.getMeasuredHeight());
            return headRect.intersect(dismissRect);
        }

        private boolean isSingleTap() { return System.currentTimeMillis() - mLastDown <= mSingleTapThreshold; }

        private boolean isOnLeftHalfOfScreen(int x) { return x < (getScreenWidth() / 2); }
    }
    //endregion





    //region View Animations
    /**
     * Align badge to the left
     * @param toLeft if true, align to left. If false, to right.
     */
    private void alignBadgeLeft(boolean toLeft) {
        RelativeLayout.LayoutParams badgeParams = (RelativeLayout.LayoutParams) mBadgeTextView.getLayoutParams();
        badgeParams.removeRule(RelativeLayout.ALIGN_END);
        badgeParams.removeRule(RelativeLayout.ALIGN_START);

        if(toLeft)
            badgeParams.addRule(RelativeLayout.ALIGN_START, popupConfigs().getRIdForFloatingViewBackground());
        else
            badgeParams.addRule(RelativeLayout.ALIGN_END, popupConfigs().getRIdForFloatingViewBackground());

        mBadgeTextView.setLayoutParams(badgeParams);
    }

    /**
     * @param toLeft true, stick floater to left edge. False, to right edge.
     */
    private void stickFloaterToLeftEdge(boolean toLeft) {
        int currentX = mFloatingParams.x;
        int targetX;
        if(toLeft) targetX = 0;
        else targetX = getScreenWidth();

        ValueAnimator va = ValueAnimator.ofInt(currentX, targetX);
        va.setDuration(popupConfigs().getEdgeStickAnimationLength());
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mFloatingParams.x = (int) animation.getAnimatedValue();
                mWindowManager.updateViewLayout(mFloatingView, mFloatingParams);
            }
        });
        va.start();
    }

    /**
     * Animate the visibility of the Dismiss view
     * @param makeVisible if true, animate the Dismiss View into sight.
     *                  if false, animate the Dismiss View out of sight.
     */
    private void setDismissViewVisibleAnimated(final boolean makeVisible, @Nullable AnimatorListenerAdapter listener) {
        int invisibleY = 0; float invisibleA = 0;
        int visibleY = popupConfigs().getDismisserBottomOffset();
        float visibleA = 1;
        int currentY, targetY; float currentA, targetA;
        if(makeVisible) {
            currentY = invisibleY; targetY = visibleY;
            currentA = invisibleA; targetA = visibleA;
        }
        else {
            currentY = visibleY; targetY = invisibleY;
            currentA = visibleA; targetA = invisibleA;
        }

        // Translation
        ValueAnimator va = ValueAnimator.ofInt(currentY, targetY);
        va.setDuration(popupConfigs().getDismissVisibilityAnimationLength());
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mDismissParams.y = (int) animation.getAnimatedValue();
                mWindowManager.updateViewLayout(mDismissView, mDismissParams);
            }
        });

        // Alpha
        ValueAnimator tva = ValueAnimator.ofFloat(currentA, targetA);
        tva.setDuration(popupConfigs().getDismissVisibilityAnimationLength());
        tva.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mDismissView.setAlpha((float) animation.getAnimatedValue());
            }
        });

        va.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if(makeVisible) mDismissView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(!makeVisible) mDismissView.setVisibility(View.GONE);
            }
        });

        if(listener != null) tva.addListener(listener);

        va.start();
        tva.start();
    }

    /**
     * @param makeVisible true means floater will be visible at the end of the animation,
     *                    false means floater will be gone at the end of the animation
     */
    private void setFloatingViewVisibleAnimated(final boolean makeVisible, @Nullable AnimatorListenerAdapter animationListener) {
        // Animate alpha
        float visibleAlpha = 1, invisibleAlpha = 0;
        float currentAlpha, targetAlpha;
        if(makeVisible) { currentAlpha = invisibleAlpha; targetAlpha = visibleAlpha; }
        else { currentAlpha = visibleAlpha; targetAlpha = invisibleAlpha; }

        ValueAnimator va = ValueAnimator.ofFloat(currentAlpha, targetAlpha);
        va.setDuration(popupConfigs().getFloaterAnimationsLength());
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mFloatingView.setAlpha((float) animation.getAnimatedValue());
            }
        });

        va.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mFloatingView.setVisibility(View.VISIBLE);
            }
            @Override
            public void onAnimationEnd(Animator animation) {
                if(!makeVisible) mFloatingView.setVisibility(View.GONE);
            }
        });

        if(animationListener != null) va.addListener(animationListener);

        va.start();
    }

    /**
     * Translate floating view to appropriate location when expanding Expanded View
     */
    private void moveFloaterOnExpanded() {
        int targetX = getScreenWidth();
        int targetY = popupConfigs().getFloaterExpandedTopOffset();

        int currentX = mFloatingParams.x;
        int currentY = mFloatingParams.y;

        ValueAnimator xa = ValueAnimator.ofInt(currentX, targetX);
        xa.setDuration(popupConfigs().getExpansionAnimationLength());
        xa.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mFloatingParams.x = (int) animation.getAnimatedValue();
                mWindowManager.updateViewLayout(mFloatingView, mFloatingParams);
            }
        });
        ValueAnimator ya = ValueAnimator.ofInt(currentY, targetY);
        ya.setDuration(popupConfigs().getExpansionAnimationLength());
        ya.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mFloatingParams.y = (int) animation.getAnimatedValue();
                mWindowManager.updateViewLayout(mFloatingView, mFloatingParams);
            }
        });

        xa.start(); ya.start();
    }

    /**
     * @param makeExpanded if true, expanded view is expanded on animation end
     *                     if false, expanded view is invisible on animation end.
     */
    private void expandExpandedViewAnimated(final boolean makeExpanded, @Nullable AnimatorListenerAdapter listener) {
        int visibleHeight = getScreenHeight() - popupConfigs().getExpandedViewTopOffset();
        int invisibleHeight = 0;

        ValueAnimator ha;
        if(makeExpanded) ha = ValueAnimator.ofInt(invisibleHeight, visibleHeight);
        else ha = ValueAnimator.ofInt(visibleHeight, invisibleHeight);
        ha.setDuration(popupConfigs().getExpansionAnimationLength());
        ha.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Attempt to fix crash "RelativeLayout not added to WindowManager
                if(mExpandedView.getParent() == null) return;

                mExpandedParams.height = (int) animation.getAnimatedValue();
                mWindowManager.updateViewLayout(mExpandedView, mExpandedParams);
            }
        });
        ha.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                // Attempt to fix crash "RelativeLayout not added to WindowManager
                if(mExpandedView.getParent() == null) return;

                mExpandedView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                // Attempt to fix crash "RelativeLayout not added to WindowManager
                if(mExpandedView.getParent() == null) return;

                if(!makeExpanded) mExpandedView.setVisibility(View.GONE);
            }
        });

        float invisibleAlpha = 0, visibleAlpha = 1;
        float currentAlpha, targetAlpha;
        if(makeExpanded) { currentAlpha = invisibleAlpha; targetAlpha = visibleAlpha; }
        else { currentAlpha = visibleAlpha; targetAlpha = invisibleAlpha; }
        ValueAnimator aa = ValueAnimator.ofFloat(currentAlpha, targetAlpha);
        aa.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Attempt to fix crash "RelativeLayout not added to WindowManager
                if(mExpandedView.getParent() == null) return;

                mExpandedView.setAlpha((float) animation.getAnimatedValue());
            }
        });

        if(listener != null) aa.addListener(listener);

        aa.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mIsExpanding = true;
            }
            @Override
            public void onAnimationEnd(Animator animation) {
                mIsExpanded = makeExpanded;
                mIsExpanding = false;
            }
        });

        ha.start();
        aa.start();
    }
    private boolean mIsExpanded = false;
    private boolean mIsExpanding = false;
    //endregion
    //endregion
}
