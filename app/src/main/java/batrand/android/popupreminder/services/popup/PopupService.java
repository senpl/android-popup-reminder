package batrand.android.popupreminder.services.popup;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.android.NativePopupService;
import batrand.android.popupreminder.android.RequestPermissionActivity;
import batrand.android.popupreminder.models.Notification;
import batrand.android.popupreminder.views.MainActivity;
import batrand.android.popupreminder.views.adapters.NotificationsAdapter;
import batrand.android.popupreminder.views.ReminderDetailActivity;
/**
 * Created by batra on 2017-05-04.
 */

public class PopupService implements IPopupService {

    //region Configurations
    @Override
    public IConfigs getConfigs() {
        return new Configs();
    }

    public class Configs implements IConfigs {
        // Classes
        @Override public Class getNativeServiceClass() { return NativePopupService.class; }
        @Override public Class getPendingOpenActivityClass() { return MainActivity.class; }
        @Override public Class getAddActivityClass() { return ReminderDetailActivity.class; }
        @Override public Class getAskPermissionActivityClass() { return RequestPermissionActivity.class; }

        // Layout XMLs
        @Override public int getLayoutRIdForFloatingView() { return R.layout.view_floating_view; }
        @Override public int getLayoutRIdForDismissView() { return R.layout.view_dismiss_view; }
        @Override public int getLayoutRIdForExpandedView() { return R.layout.view_floating_expanded; }

        @Override public int getRIdForFloatingViewBackground() { return R.id.floating_view_background; }
        @Override public int getRIdForFloatingViewClosingBackground() { return R.id.floating_view_background_close; }

        @Override public int getRIdForBadgeTextView() { return R.id.floating_view_badge; }
        @Override public int getRIdForExpandedRecyclerEmptyable() { return R.id.floating_expanded_recycler; }
        @Override public int getRIdForExpandedEmptyView() { return R.id.floating_expanded_empty; }

        // Appearance
        @Override
        public NotificationsAdapter getExpandedViewAdapter(Context context) {
            return new NotificationsAdapter(context, null);
        }

        @Override public int getEdgeStickAnimationLength() { return 100; }
        @Override public int getDismissVisibilityAnimationLength() { return 100; }
        @Override public int getFloaterAnimationsLength() { return 100; }
        @Override public int getFloaterExpandedTopOffset() { return 100; }
        @Override public int getExpandedViewTopOffset() { return 400; }
        @Override public int getExpansionAnimationLength() { return 300; }

        @Override public int getStartingFloaterTopOffset() { return 100; }
        @Override public int getDismisserBottomOffset() { return 100; }

        // Actions
        @Override public String getCommandKey() { return "command"; }
        @Override public String getDismissCommand() { return "dismiss"; }
        @Override public Intent getDismissIntent(Context fromContext) { return getServiceIntentWithCommand(fromContext, getDismissCommand()); }
        @Override public String getPopupCommand() { return "popup"; }
        @Override public String getPopupCommand(String notificationId) { return getPopupCommand() + notificationId; }
        @Override public String getNotificationIdFromCommand(String popupCommand) {
            if(!popupCommand.startsWith(getPopupCommand())) return "";
            return popupCommand.replace(getPopupCommand(),"");
        }

        @Override public int getVibrationDuration() { return 500; }
    }
    //endregion

    @Override
    public void startServiceOrProcessPermission(Context fromContext) {
        if(hasPermission(fromContext))
            fromContext.startService(getServiceIntent(fromContext));
        else processPermission(fromContext);
    }

    @Override
    public void processPermission(Context context) {
        if(hasPermission(context)) return;
        Intent intent = new Intent(context, getConfigs().getAskPermissionActivityClass());
        context.startActivity(intent);
    }

    @Override
    public boolean hasPermission(Context fromContext) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return true;
        else return Settings.canDrawOverlays(fromContext);
    }

    private Intent getServiceIntent(Context fromContext) {
        return new Intent(fromContext, getConfigs().getNativeServiceClass());
    }

    private Intent getServiceIntentWithCommand(Context fromContext, String command) {
        Intent intent = getServiceIntent(fromContext);
        intent.putExtra(getConfigs().getCommandKey(), command);
        return intent;
    }

    @Override
    public void stopService(Context fromContext) {
        fromContext.stopService(new Intent(fromContext, getConfigs().getNativeServiceClass()));
    }

    @Override
    public void popup(Context fromContext, String notificationId) {
        Intent intent = getServiceIntentWithCommand(fromContext, getConfigs().getPopupCommand(notificationId));

        // On Oreo+, it is required to use startForegroundService
        // with implicit promise to start a foreground notification
        // within the ANR timeout.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fromContext.startForegroundService(intent);
        }
        else {
            fromContext.startService(intent);
        }
    }
}
