package batrand.android.popupreminder.services.formatter;

import android.content.Context;

/**
 * Created by batra on 2017-05-23.
 */

public interface IFormatService {
    String formatTime24(int hourOfDay, int minute);
    String formatTime12(int hourOfDay, int minute);
    String formatDate(int year, int monthOfYear, int dayOfMonth);
    String formatDateTime(Context context, long unixTime);
    String formatDateTimeCompact(Context context, long unixTime);
}
