package batrand.android.popupreminder.services.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import batrand.android.popupreminder.R;

/**
 * Created by batra on 2017-07-04.
 */

public class SettingsService implements ISettingsService {
    @Override
    public boolean shouldPlayPopupSound(Context context) {
        return getPrefBoolean(context,
                R.string.prefkey_pref_popup_sound_enabled,
                R.bool.default_popup_sound_enabled
        );
    }

    @Override
    public int getPopupSoundRawRId(Context context) {
        String popupSound = getPrefString(context, R.string.prefkey_pref_popup_sound, R.string.default_popup_sound);

        // TODO: Don't know how not to hard code these...
        if(popupSound.equals("ping_ding")) return R.raw.richerlandtv_pingding;
        if(popupSound.equals("message_ding")) return R.raw.robinhood76_messageding;
        return 0;
    }

    @Override
    public boolean shouldAutoMarkSeen(Context context) {
        return getPrefBoolean(context,
                R.string.prefkey_pref_auto_mark_seen,
                R.bool.default_auto_mark_seen
        );
    }

    @Override
    public boolean remindersCanBeInexact(Context context) {
        return getPrefBoolean(context,
                R.string.prefkey_pref_reminders_can_be_inexact,
                R.bool.default_reminders_can_be_inexact);
    }

    @Override
    public boolean shouldPrefillWithCurrentTime(Context context) {
        return getPrefBoolean(context,
                R.string.prefkey_pref_prefill_with_current_time,
                R.bool.default_prefill_with_current_time
        );
    }

    @Override
    public int getDefaultInterval(Context context) {
        return getPrefInt(context, R.string.prefkey_pref_default_interval, R.string.default_interval);
    }

    @Override
    public int getDefaultIntervalType(Context context) {
        return getPrefInt(context, R.string.prefkey_pref_default_interval_type, R.string.default_interval_type);
    }

    @Override
    public float getSoundVolume(Context context) {
        // Material Seekbar stores integers, not strings
        int volInt = getSharedPrefs(context).getInt(
                getString(context, R.string.prefkey_pref_popup_sound_vol),
                Integer.parseInt(getString(context, R.string.default_popup_sound_volume))
        );
        return ((float)volInt)/100f;
    }

    @Override
    public boolean getShouldUse24hTime(Context context) {
        return getPrefBoolean(context, R.string.prefkey_pref_use_24h_time, R.bool.default_use_24h_time);
    }

    @Override
    public boolean getShouldReportCrashes(Context context) {
        return getPrefBoolean(context, R.string.prefkey_pref_should_report_crashes, R.bool.default_should_report_crashes);
    }

    @Override
    public boolean getHasSeenInternetPermissionRationale(Context context) {
        return getPrefBoolean(context, R.string.prefkey_pref_has_seen_internet_permission_rationale,
                R.bool.default_has_seen_internet_permission_rationale);
    }

    @Override
    public void setHasSeenInternetPermissionRationale(Context context, boolean hasSeen) {
        SharedPreferences.Editor editor = getSharedPrefs(context).edit();
        editor.putBoolean(getString(context, R.string.prefkey_pref_has_seen_internet_permission_rationale), hasSeen);
        editor.apply();
    }

    @Override
    public boolean getShouldVibrate(Context context) {
        return getPrefBoolean(context, R.string.prefkey_pref_should_vibrate, R.bool.default_should_vibrate);
    }

    private SharedPreferences getSharedPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private String getString(Context context, int prefKeyRId) {
        return context.getString(prefKeyRId);
    }

    private boolean getBoolean(Context context, int RId) {
        return context.getResources().getBoolean(RId);
    }

    private boolean getPrefBoolean(Context context, int prefKeyRId, int defaultRId) {
        return getSharedPrefs(context).getBoolean(
                getString(context, prefKeyRId),
                getBoolean(context, defaultRId)
        );
    }

    private String getPrefString(Context context, int prefKeyRId, int defaultRId) {
        return getSharedPrefs(context).getString(
                getString(context, prefKeyRId),
                getString(context, defaultRId)
        );
    }

    private int getPrefInt(Context context, int prefKeyRId, int defaultRId) {
        String string = getPrefString(context, prefKeyRId, defaultRId);
        return Integer.parseInt(string);
    }
}
