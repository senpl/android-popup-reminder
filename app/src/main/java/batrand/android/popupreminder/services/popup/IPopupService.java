package batrand.android.popupreminder.services.popup;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import batrand.android.popupreminder.models.Notification;
import batrand.android.popupreminder.views.adapters.NotificationsAdapter;

/**
 * Created by batra on 2017-05-03.
 */

public interface IPopupService {

    IConfigs getConfigs();

    void processPermission(Context fromContext);
    boolean hasPermission(Context fromContext);

    /**
     * Either startService the service, or show the permission activity
     */
    void startServiceOrProcessPermission(Context fromContext);

    void stopService(Context fromContext);

    /**
     * Send a popup associated with this Notification to the screen
     */
    void popup(Context fromContext, String notificationId);

    /**
     * Created by batra on 2017-05-08.
     */

    interface IConfigs {
        //region Related Classes
        /**
         * @return the class of the Android Service class
         */
        Class getNativeServiceClass();

        /**
         * @return the class of the activity to be opened
         * when clicking on the foreground notification
         */
        Class getPendingOpenActivityClass();

        Class getAddActivityClass();

        /**
         * @return the class of the Activity that will handle permissions
         */
        Class getAskPermissionActivityClass();
        //endregion

        //region Layout XMLs
        /**
         * @return the Layout ID that defines the floating view
         */
        int getLayoutRIdForFloatingView();

        /**
         * @return the Layout ID that defines the dismiss view
         */
        int getLayoutRIdForDismissView();

        /**
         * @return the Layout ID that defines the expanded view
         */
        int getLayoutRIdForExpandedView();

        /**
         * @return the ID of the background circle
         */
        int getRIdForFloatingViewBackground();

        /**
         * @return the ID of the background circle for closing the expanded view
         */
        int getRIdForFloatingViewClosingBackground();

        /**
         * @return the ID of the TextView that is used for the unread badge
         */
        int getRIdForBadgeTextView();

        /**
         * @return the ID of the RecyclerView that extends BaseEmptyableRecyclerView
         */
        int getRIdForExpandedRecyclerEmptyable();

        /**
         * @return ID for the Empty view for the recycler
         */
        int getRIdForExpandedEmptyView();
        //endregion

        //region Appearance
        NotificationsAdapter getExpandedViewAdapter(Context context);

        int getEdgeStickAnimationLength();
        int getDismissVisibilityAnimationLength();
        int getFloaterAnimationsLength();

        /**
         * @return Floating View's top offset when expanded view is expanded
         */
        int getFloaterExpandedTopOffset();

        int getExpandedViewTopOffset();

        /**
         * @return animation length of expanding the expanded view
         */
        int getExpansionAnimationLength();

        /**
         * @return distance fromt he top of floater away from the top when the popup starts
         */
        int getStartingFloaterTopOffset();

        /**
         * @return distance from the bottom of dismisser away from the bottom of the screen
         */
        int getDismisserBottomOffset();
        //endregion

        //region Commands

        String getCommandKey();
        String getDismissCommand();
        Intent getDismissIntent(Context fromContext);
        String getPopupCommand();
        String getPopupCommand(String notificationId);
        String getNotificationIdFromCommand(String popupCommand);

        //endregion

        int getVibrationDuration();
    }
}
