package batrand.android.popupreminder.services.log;

import java.util.List;
import java.util.concurrent.TimeUnit;

import batrand.android.popupreminder.models.LogEntry;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by batra on 2017-06-07.
 */

public class Log implements ILog {
    public Log() {
        clearOldLogs();
    }

    @Override
    public void log(final String message) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm tRealm) {
                LogEntry entry = new LogEntry();
                entry.message = message;
                tRealm.copyToRealm(entry);
            }
        });
        realm.close();
    }

    @Override
    public void log(Realm realmInstance, String message) {
        LogEntry entry = new LogEntry();
        entry.message = message;
        realmInstance.copyToRealm(entry);
    }

    @Override
    public List<LogEntry> getAllEntries() {
        return getAllEntriesSortedByTimestamp(Sort.DESCENDING);
    }

    @Override
    public List<LogEntry> getAllEntriesSortedByTimestamp(Sort sortOrder) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<LogEntry> results = realm.where(LogEntry.class)
                .sort("timestamp", Sort.ASCENDING).findAll();
        List<LogEntry> entries = realm.copyFromRealm(results);
        realm.close();
        return entries;
    }

    @Override
    public void clearLogs() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm tRealm) {
                tRealm.where(LogEntry.class).findAll().deleteAllFromRealm();
            }
        });
        realm.close();
    }

    private void clearOldLogs() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm tRealm) {
                tRealm.where(LogEntry.class)
                        // Logs with timestamp older than 60 days
                        .lessThan("timestamp", System.currentTimeMillis() - TimeUnit.DAYS.toMillis(60))
                        .findAll()
                        .deleteAllFromRealm();
            }
        });
        realm.close();
    }
}
