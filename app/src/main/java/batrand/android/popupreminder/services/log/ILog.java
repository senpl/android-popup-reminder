package batrand.android.popupreminder.services.log;

import java.util.List;

import batrand.android.popupreminder.models.LogEntry;
import io.realm.Realm;
import io.realm.Sort;

/**
 * Created by batra on 2017-06-07.
 */

public interface ILog {
    /**
     * Save log message. Will create new Realm instance, execute, then close.
     */
    void log(String message);

    /**
     * Save log message using passed in Realm instance directly.
     * For use inside another Realm transaction.
     */
    void log(Realm realmInstance, String message);

    List<LogEntry> getAllEntries();
    List<LogEntry> getAllEntriesSortedByTimestamp(Sort sortOrder);
    void clearLogs();
}
