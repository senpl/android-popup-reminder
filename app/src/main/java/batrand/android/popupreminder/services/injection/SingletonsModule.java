package batrand.android.popupreminder.services.injection;

import javax.inject.Singleton;

import batrand.android.popupreminder.services.alarm.AlarmService;
import batrand.android.popupreminder.services.alarm.IAlarmService;
import batrand.android.popupreminder.services.formatter.FormatService;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.log.ILog;
import batrand.android.popupreminder.services.log.Log;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.popup.IPopupSoundService;
import batrand.android.popupreminder.services.popup.PopupService;
import batrand.android.popupreminder.services.popup.PopupSoundService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.services.reminder.ReminderService;
import batrand.android.popupreminder.services.settings.ISettingsService;
import batrand.android.popupreminder.services.settings.SettingsService;
import dagger.Module;
import dagger.Provides;

/**
 * Created by batra on 2017-05-09.
 */
@Module
public class SingletonsModule {
    @Provides @Singleton static ILog provideLog() { return new Log(); }
    @Provides @Singleton static IAlarmService provideAlarmService() { return new AlarmService(); }
    @Provides @Singleton static IPopupService providePopupService() { return new PopupService(); }
    @Provides @Singleton static IPopupSoundService providePopupSoundService() { return new PopupSoundService(); }
    @Provides @Singleton static IReminderService provideReminderService(IAlarmService alarm, IPopupService popup, ILog log, IFormatService format) {
        return new ReminderService(alarm, popup, log, format);
    }
    @Provides @Singleton static ISettingsService provideSettingsService() { return new SettingsService(); }
    @Provides @Singleton static IFormatService provideFormatService(ISettingsService settings) { return new FormatService(settings); }
}
