package batrand.android.popupreminder.services.settings;

import android.content.Context;

/**
 * Created by batra on 2017-07-04.
 */

public interface ISettingsService {
    boolean shouldPlayPopupSound(Context context);
    int getPopupSoundRawRId(Context context);
    boolean shouldAutoMarkSeen(Context context);
    boolean remindersCanBeInexact(Context context);
    boolean shouldPrefillWithCurrentTime(Context context);
    int getDefaultInterval(Context context);
    int getDefaultIntervalType(Context context);
    float getSoundVolume(Context context);
    boolean getShouldUse24hTime(Context context);
    boolean getShouldReportCrashes(Context context);
    boolean getHasSeenInternetPermissionRationale(Context context);
    void setHasSeenInternetPermissionRationale(Context context, boolean hasSeen);
    boolean getShouldVibrate(Context context);
}
