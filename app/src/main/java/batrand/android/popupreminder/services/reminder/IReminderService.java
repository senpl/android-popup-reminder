package batrand.android.popupreminder.services.reminder;

import android.content.Context;

import java.util.List;

import batrand.android.popupreminder.models.Notification;
import batrand.android.popupreminder.models.Subtask;
import batrand.android.popupreminder.models.reminder.Reminder;

/**
 * Created by batra on 2017-05-09.
 */

public interface IReminderService {

    IConfigs getConfigs();

    /**
     * @return true when registration succeeds.
     */
    boolean registerReminder(Context context, Reminder reminder);
    void unregisterReminder(Context context, String reminderId);
    Reminder getReminder(String reminderId);
    List<Reminder> getAllReminders();

    /**
     * Called by the trigger listener service when a reminder's alarm went off.
     */
    void onReminderTriggered(Context fromContext, String triggeredReminderId);

    Notification getNotification(String notificationId);

    List<Notification> getNextActionNotification();

    List<Notification> getAllNotifications();
    List<Notification> getUnseenNotifications();
    List<Notification> getSeenNotifications();
    List<Notification> getAllNotHiddenNotifications();

    int getNotHiddenNotificationCount();
    int getUnseenNotificationCount();

    void markNotificationSeen(Context context, String notificationId);
    void markAllNotificationsSeen(Context context);
    void deleteNotification(Context context, String notificationId);

    /**
     * Remove all alarms and set them up again
     */
    void resetAlarms(Context context);

    List<Subtask> getSubtasksForRemainder(String remainderId);

    void remveSubtask(Context mContext, final String title);

    interface IConfigs {
        /**
         * @return the string that is broadcasted when new notifications are available
         */
        String getChangeAvailableBroadcastString();

        /**
         * @return the millisecond-equivalent to the minimum acceptable interval
         * Example: if min interval is 10 minutes, return 600000
         */
        long getMinimumIntervalMillis();

        /**
         * @return String Resource ID of a readable format of the minimum interval
         * Example: R.id.min_interval, to a string of "10 minutes"
         */
        int getStringRIdOfReadableMinimumInterval();

        /**
         * @return the millisecond-equivalent to the minimum acceptable interval
         * Example: if max interval is 1 year, return 31557600000
         */
        long getMaximumIntervalMillis();

        /**
         * @return String Resource ID of a readable format of the maximum interval
         * Example: R.id.max_interval, to a string of "1 year"
         */
        int getStringRIdOfReadableMaximumInterval();

        /**
         * @return true when the interval is within range of min-max intervals
         */
        boolean isValidInterval(long interval);
    }
}
