package batrand.android.popupreminder.services.formatter;

import android.content.Context;

import java.util.Calendar;

import javax.inject.Inject;

import batrand.android.popupreminder.services.settings.ISettingsService;

/**
 * Created by batra on 2017-05-23.
 */

public class FormatService implements IFormatService {
    private static final String mTimeSeparator = " : ";
    private static final String mDateSeparator = " - ";

    private ISettingsService mSettings;
    @Inject public FormatService(ISettingsService settings) {
        mSettings = settings;
    }

    @Override
    public String formatTime24(int hourOfDay, int minute) {
        String hour = Integer.toString(hourOfDay);
        if(hour.length() == 1) hour = "0" + hour;

        String minuteString = Integer.toString(minute);
        if(minuteString.length() == 1) minuteString = "0" + minuteString;

        return hour + mTimeSeparator + minuteString;
    }

    @Override
    public String formatTime12(int hourOfDay, int minute) {
        boolean isPM = false;
        if(hourOfDay > 12) {
            isPM = true;
            hourOfDay -= 12;
        }

        String hour = Integer.toString(hourOfDay);
        if(hour.length() == 1) hour = "0" + hour;

        String minuteString = Integer.toString(minute);
        if(minuteString.length() == 1) minuteString = "0" + minuteString;

        String time = hour + mTimeSeparator + minuteString;

        if(isPM) time += " PM";
        else time += " AM";

        return time;
    }

    @Override
    public String formatDate(int year, int monthOfYear, int dayOfMonth) {
        monthOfYear += 1;

        String month = Integer.toString(monthOfYear);
        if(month.length() == 1) month = "0" + month;

        String day = Integer.toString(dayOfMonth);
        if(day.length() == 1) day = "0" + day;

        return year + mDateSeparator + month + mDateSeparator + day;
    }

    @Override
    public String formatDateTime(Context context, long unixTime) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(unixTime);
        String date = formatDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        String time;
        if(shouldUse24h(context)) time = formatTime24(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
        else time = formatTime12(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));

        return date + " " + time;
    }

    @Override
    public String formatDateTimeCompact(Context context, long unixTime) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(unixTime);
        String date = formatDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        String time;
        if(shouldUse24h(context)) time = formatTime24(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
        else time = formatTime12(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));

        String dateCompact = date.replaceAll("\\s+","");
        String timeCompact = time.replaceAll("\\s+","");

        return dateCompact + " " + timeCompact;
    }

    private boolean shouldUse24h(Context context) {
        return mSettings.getShouldUse24hTime(context);
    }
}
