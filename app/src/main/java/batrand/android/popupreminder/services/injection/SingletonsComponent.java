package batrand.android.popupreminder.services.injection;

import javax.inject.Singleton;

import batrand.android.popupreminder.android.AlarmTriggerReceiver;
import batrand.android.popupreminder.android.BootCompletedReceiver;
import batrand.android.popupreminder.android.NativePopupService;
import batrand.android.popupreminder.android.PackageReplacedReceiver;
import batrand.android.popupreminder.android.PopupReminderApplication;
import batrand.android.popupreminder.services.alarm.AlarmService;
import batrand.android.popupreminder.services.popup.PopupSoundService;
import batrand.android.popupreminder.views.InternetRationaleActivity;
import batrand.android.popupreminder.views.LogActivity;
import batrand.android.popupreminder.views.MainActivity;
import batrand.android.popupreminder.views.NotificationsFragment;
import batrand.android.popupreminder.views.SeenFragment;
import batrand.android.popupreminder.views.ReminderDetailActivity;
import batrand.android.popupreminder.views.RemindersFragment;
import batrand.android.popupreminder.views.SettingsActivity;
import batrand.android.popupreminder.views.adapters.LogsAdapter;
import batrand.android.popupreminder.views.adapters.NotificationsAdapter;
import batrand.android.popupreminder.views.adapters.SubtasksAdapter;
import batrand.android.popupreminder.views.adapters.SeenNotificationsAdapter;
import batrand.android.popupreminder.views.adapters.RemindersAdapter;
import dagger.Component;

/**
 * Created by batra on 2017-05-09.
 */
@Singleton @Component(modules = SingletonsModule.class)
public interface SingletonsComponent {
    void inject(PopupSoundService service);
    void inject(AlarmService service);

    void inject(NativePopupService service);

    void inject(MainActivity activity);
    void inject(ReminderDetailActivity activity);
    void inject(LogActivity activity);
    void inject(InternetRationaleActivity activity);

    void inject(NotificationsFragment fragment);
    void inject(SeenFragment fragment);

    void inject(RemindersFragment fragment);
    void inject(SettingsActivity.SettingsFragment fragment);

    void inject(NotificationsAdapter adapter);
    void inject(SeenNotificationsAdapter adapter);

    void inject(LogsAdapter adapter);
    void inject(RemindersAdapter adapter);

    void inject(PopupReminderApplication application);

    void inject(BootCompletedReceiver receiver);
    void inject(PackageReplacedReceiver receiver);
    void inject(AlarmTriggerReceiver receiver);

    void inject(SubtasksAdapter subtasksAdapter);
}
