package batrand.android.popupreminder.services.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.Nullable;

import java.util.Map;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.android.AlarmTriggerReceiver;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.log.ILog;
import batrand.android.popupreminder.services.settings.ISettingsService;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

/**
 * Created by batra on 2017-05-03.
 */

public class AlarmService implements IAlarmService {

    @Inject ILog mLog;
    @Inject IFormatService mFormatter;
    @Inject ISettingsService mSettings;

    public AlarmService() {
        getInjector().inject(this);
    }

    @Override public IConfigs getConfigs() { return new Configs(); }
    public static class Configs implements IConfigs {
        @Override public String getTriggerAction(Context context) { return context.getString(R.string.alarm_trigger_action); }
        @Override public Class getTriggerReceiverClass() { return AlarmTriggerReceiver.class; }
        @Override public int getRequestCode() { return 15872; }
        @Override public String getIdKey() { return "id"; }
    }

    @Override
    public void setAlarm(Context context, String reminderId, long alarmTimeUtc, @Nullable Map<String,String> extras) {
        PendingIntent pending = getPendingIntent(context, reminderId, PendingIntent.FLAG_CANCEL_CURRENT, extras);

        if(mSettings.remindersCanBeInexact(context)) {
            getAlarmManager(context).set(AlarmManager.RTC_WAKEUP, alarmTimeUtc, pending);
        }
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getAlarmManager(context).setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTimeUtc, pending);
            }
            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getAlarmManager(context).setExact(AlarmManager.RTC_WAKEUP, alarmTimeUtc, pending);
            }
            else {
                getAlarmManager(context).set(AlarmManager.RTC_WAKEUP, alarmTimeUtc, pending);
            }
        }
    }

    @Override
    public void cancelAlarm(Context context, String reminderId) {
        if(!isAlarmSet(context, reminderId)) return;
        PendingIntent pending = getPendingIntent(context, reminderId, PendingIntent.FLAG_NO_CREATE, null);
        getAlarmManager(context).cancel(pending);
        pending.cancel();
    }

    @Override
    public boolean isAlarmSet(Context context, String reminderId) {
        return getPendingIntent(context, reminderId, PendingIntent.FLAG_NO_CREATE, null) != null;
    }

    private AlarmManager getAlarmManager(Context context) {
        return (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    private PendingIntent getPendingIntent(Context context, String uid, int pendingIntentFlag, @Nullable Map<String,String> extras) {
        Intent intent = new Intent();
        intent.setAction(getConfigs().getTriggerAction(context));
        intent.setClass(context, getConfigs().getTriggerReceiverClass());

        intent.putExtra(getConfigs().getIdKey(),uid);

        if(extras != null) {
            for(String key : extras.keySet()) {
                intent.putExtra(key, extras.get(key));
            }
        }

        return PendingIntent.getBroadcast(context, getConfigs().getRequestCode(), intent, pendingIntentFlag);
    }

}
