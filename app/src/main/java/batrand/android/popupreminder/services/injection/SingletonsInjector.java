package batrand.android.popupreminder.services.injection;

/**
 * Created by batra on 2017-05-10.
 */

public class SingletonsInjector {
    public static SingletonsComponent getInjector() {
        return DaggerSingletonsComponent.create();
    }
}
