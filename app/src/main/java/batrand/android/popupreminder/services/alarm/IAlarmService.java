package batrand.android.popupreminder.services.alarm;

import android.content.Context;

import androidx.annotation.Nullable;

import java.util.Map;

/**
 * Created by batra on 2017-05-03.
 */

public interface IAlarmService {

    IConfigs getConfigs();

    /**
     * Set an alarm that will broadcasts an Intent with a unique Action
     * @param reminderId the ID of the reminder that this alarm is for
     * @param alarmTimeUtc the time the alarm should fire, in System.currentTimeMillis()
     * @param extras String key - String value that will be attached as the Intent's extras
     */
    void setAlarm(Context context, String reminderId, long alarmTimeUtc, @Nullable Map<String,String> extras);

    /**
     * Cancel the alarm of the Reminder with this ID
     */
    void cancelAlarm(Context context, String reminderId);

    /**
     * Check if an alarm for the Reminder with this ID is set.
     * @return true if there is such an alarm set.
     */
    boolean isAlarmSet(Context context, String reminderId);

    interface IConfigs {
        /**
         * @return get the Intent action string that will invoke the alarm trigger
         * broadcast receiver.
         */
        String getTriggerAction(Context context);
        Class getTriggerReceiverClass();
        int getRequestCode();
        String getIdKey();
    }
}
